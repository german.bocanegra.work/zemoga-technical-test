﻿using GBW.Zemoga.TechnicalTest.Data.EntityDefinitions;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using Microsoft.EntityFrameworkCore;

namespace GBW.Zemoga.TechnicalTest.Data.UnitsOfWork.EntityFramework.Contexts
{
    public class MainDBContext : DbContext
    {
        public MainDBContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<PostReviewAction> PostReviewActions { get; set; }
        public DbSet<PostState> PostStates { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<AccessToken> AccessTokens { get; set; }

        public DbSet<Post> Posts { get; set; }
        public DbSet<PostComment> PostComments { get; set; }
        public DbSet<PostReview> PostReviews { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PostEntityDefinition).Assembly);
        }
    }
}
