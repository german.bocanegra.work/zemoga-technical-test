﻿using GBW.Zemoga.TechnicalTest.Data.Repositories;
using GBW.Zemoga.TechnicalTest.Data.UnitsOfWork.EntityFramework.Contexts;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.Repositories;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;

namespace GBW.Zemoga.TechnicalTest.Data.UnitsOfWork
{
    public class MainUnitOfWork : IUnitOfWork
    {
        private readonly MainDBContext _mainContext;
        private readonly UserInformation _userInformation;

        public IRepository<Post> Posts { get; }
        public IRepository<PostComment> PostComments { get; }
        public IRepository<PostReview> PostReviews { get; }
        public IRepository<User> Users { get; }

        public IRepository<PostState> PostStates { get; }
        public IRepository<PostReviewAction> PostReviewActions { get; }
        public IRepository<UserRole> UserRoles { get; }
        public IRepository<AccessToken> AccessTokens { get; }

        public MainUnitOfWork(MainDBContext mainContext, UserInformation userInformation)
        {
            _mainContext = mainContext;
            _userInformation = userInformation;

            Posts = new GenericRepository<Post>(_mainContext, _userInformation);
            PostComments = new GenericRepository<PostComment>(_mainContext, _userInformation);
            PostReviews = new GenericRepository<PostReview>(_mainContext, _userInformation);
            Users = new GenericRepository<User>(_mainContext, _userInformation);

            PostStates = new GenericRepository<PostState>(_mainContext, _userInformation);
            PostReviewActions = new GenericRepository<PostReviewAction>(_mainContext, _userInformation);
            UserRoles = new GenericRepository<UserRole>(_mainContext, _userInformation);
            AccessTokens = new GenericRepository<AccessToken>(_mainContext, _userInformation);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _mainContext.SaveChangesAsync();
        }
    }
}
