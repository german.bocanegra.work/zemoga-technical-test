﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GBW.Zemoga.TechnicalTest.Data.EntityDefinitions
{
    public class PostReviewActionEntityDefinition : IEntityTypeConfiguration<PostReviewAction>
    {
        public void Configure(EntityTypeBuilder<PostReviewAction> builder)
        {
            builder
                .ToTable(nameof(PostReviewAction));

            builder
                .HasKey(x => x.ID);

            builder
                .Property(x => x.Name)
                .HasMaxLength(15)
                .IsRequired();

            builder
                .HasData(SeededData);
        }

        public ICollection<PostReviewAction> SeededData
        {
            get
            {
                return new List<PostReviewAction>
                {
                    new PostReviewAction
                    {
                        ID = PostReviewActionConstants.ApprovePostActionID,
                        Name = PostReviewActionConstants.ApprovePostActionName
                    },
                    new PostReviewAction
                    {
                        ID = PostReviewActionConstants.RejectPostActionID,
                        Name = PostReviewActionConstants.RejectPostActionName
                    },
                };
            }
        }
    }
}
