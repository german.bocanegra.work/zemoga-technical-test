﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GBW.Zemoga.TechnicalTest.Data.EntityDefinitions
{
    public class PostReviewEntityDefinition : IEntityTypeConfiguration<PostReview>
    {
        public void Configure(EntityTypeBuilder<PostReview> builder)
        {
            builder
                .ToTable(nameof(PostReview));

            builder
                .HasKey(x => x.ID);

            builder
                .Property(x => x.PostID)
                .IsRequired();

            builder
                .Property(x => x.ReviewerID)
                .IsRequired();

            builder
                .Property(x => x.ActionID)
                .IsRequired();

            builder
                .Property(x => x.RejectionComment)
                .HasMaxLength(500)
                .IsRequired(false);

            builder.HasOne(x => x.Post);
            builder.HasOne(x => x.Reviewer);
            builder.HasOne(x => x.Action);
        }
    }
}
