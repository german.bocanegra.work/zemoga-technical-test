﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GBW.Zemoga.TechnicalTest.Data.EntityDefinitions
{
    public class AccessTokenEntityDefinition : IEntityTypeConfiguration<AccessToken>
    {
        public void Configure(EntityTypeBuilder<AccessToken> builder)
        {
            builder
                .ToTable(nameof(AccessToken));

            builder
                .HasKey(x => x.ID);

            builder
                .Property(x => x.UserID)
                .IsRequired();

            builder
                .Property(x => x.UserName)
                .HasMaxLength(60)
                .IsRequired();

            builder
                .Property(x => x.LastRefreshTimestamp)
                .IsRequired();

            builder
                .Property(x => x.TokenDurationInMinutes)
                .IsRequired();

            builder.HasOne(x => x.User);
        }
    }
}
