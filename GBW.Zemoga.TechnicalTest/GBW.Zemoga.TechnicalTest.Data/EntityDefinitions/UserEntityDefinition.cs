﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GBW.Zemoga.TechnicalTest.Data.EntityDefinitions
{
    public class UserEntityDefinition : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .ToTable(nameof(User));

            builder
                .HasKey(x => x.ID);

            builder
                .Property(x => x.UserName)
                .HasMaxLength(60)
                .IsRequired();

            builder
                .HasIndex(e => e.UserName)
                .IsUnique();

            builder
                .Property(x => x.DisplayName)
                .HasMaxLength(60)
                .IsRequired();

            builder
                .Property(x => x.Password)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(x => x.RoleID)
                .IsRequired();

            builder
                .Property(x => x.IsActive)
                .IsRequired();

            builder.HasOne(x => x.Role);

            builder
                .HasData(SeededData);
        }

        public ICollection<User> SeededData
        {
            get
            {
                return new List<User>
                {
                    new User
                    {
                        ID = new Guid("83f4730a-3ed0-4693-b015-1d0abdf2f558"),
                        SortOrder = 1,

                        UserName = "dev_admin",
                        DisplayName = "Development Administrator",
                        Password = "56f862b6-133d-4a21-a3e0-41206ba325ea",

                        RoleID = UserRolesConstants.AdministratorRoleID,
                        IsActive = true
                    },

                    new User
                    {
                        ID = new Guid("b5f846f6-fe27-4e1a-b787-2e658984c4f1"),
                        SortOrder = 2,

                        UserName = "writer_1",
                        DisplayName = "Writer 1",
                        Password = "1",

                        RoleID = UserRolesConstants.WriterRoleID,
                        IsActive = true
                    },
                    new User
                    {
                        ID = new Guid("3d06d0b7-6be2-4eff-8528-adccef660bed"),
                        SortOrder = 3,

                        UserName = "writer_2",
                        DisplayName = "Writer 2",
                        Password = "2",

                        RoleID = UserRolesConstants.WriterRoleID,
                        IsActive = true
                    },

                    new User
                    {
                        ID = new Guid("23be9e88-62a2-48fe-acc0-3e9eb8294040"),
                        SortOrder = 4,

                        UserName = "editor_1",
                        DisplayName = "Editor 1",
                        Password = "3",

                        RoleID = UserRolesConstants.EditorRoleID,
                        IsActive = true
                    },
                    new User
                    {
                        ID = new Guid("26981d11-59dd-4e76-95cb-cb8309f72e6a"),
                        SortOrder = 5,

                        UserName = "editor_2",
                        DisplayName = "Editor 2",
                        Password = "4",

                        RoleID = UserRolesConstants.EditorRoleID,
                        IsActive = true
                    },

                    new User
                    {
                        ID = new Guid("98771b2c-72a1-4461-beeb-f2c1ac67e1cf"),
                        SortOrder = 6,

                        UserName = "public_1",
                        DisplayName = "Public 1",
                        Password = "5",

                        RoleID = UserRolesConstants.PublicRoleID,
                        IsActive = true
                    },
                    new User
                    {
                        ID = new Guid("de3c1355-960b-40e9-9374-cdc97c71806f"),
                        SortOrder = 7,

                        UserName = "public_2",
                        DisplayName = "Public 2",
                        Password = "6",

                        RoleID = UserRolesConstants.PublicRoleID,
                        IsActive = true
                    }
                };
            }
        }
    }
}
