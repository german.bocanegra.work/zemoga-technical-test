﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GBW.Zemoga.TechnicalTest.Data.EntityDefinitions
{
    public class PostCommentEntityDefinition : IEntityTypeConfiguration<PostComment>
    {
        public void Configure(EntityTypeBuilder<PostComment> builder)
        {
            builder
                .ToTable(nameof(PostComment));

            builder
                .HasKey(x => x.ID);

            builder
                .Property(x => x.PostID)
                .IsRequired();

            builder
                .Property(x => x.CommenterID)
                .IsRequired();

            builder
                .Property(x => x.Title)
                .HasMaxLength(60)
                .IsRequired();

            builder
                .Property(x => x.Content)
                .HasMaxLength(500)
                .IsRequired();

            builder.HasOne(x => x.Post);
            builder.HasOne(x => x.Commenter);
        }
    }
}
