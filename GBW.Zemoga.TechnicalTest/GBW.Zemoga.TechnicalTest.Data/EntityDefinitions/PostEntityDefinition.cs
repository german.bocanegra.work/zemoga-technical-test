﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GBW.Zemoga.TechnicalTest.Data.EntityDefinitions
{
    public class PostEntityDefinition : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder
                .ToTable(nameof(Post));

            builder
                .HasKey(x => x.ID);

            builder
                .Property(x => x.OwnerUserID)
                .IsRequired();

            builder
                .Property(x => x.Title)
                .HasMaxLength(60)
                .IsRequired();

            builder
                .Property(x => x.Content)
                .HasMaxLength(500)
                .IsRequired();

            builder
                .Property(x => x.StatusID)
                .IsRequired();

            builder
                .Property(x => x.SubmitDate)
                .IsRequired(false);

            builder
                .Property(x => x.PublishDate)
                .IsRequired(false);

            builder.HasOne(x => x.OwnerUser);
            builder.HasMany(x => x.Reviews).WithOne(y => y.Post);
            builder.HasMany(x => x.Comments).WithOne(y => y.Post);
        }
    }
}
