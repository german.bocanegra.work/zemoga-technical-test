﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GBW.Zemoga.TechnicalTest.Data.EntityDefinitions
{
    public class PostStateEntityDefinition : IEntityTypeConfiguration<PostState>
    {
        public void Configure(EntityTypeBuilder<PostState> builder)
        {
            builder
                .ToTable(nameof(PostState));

            builder
                .HasKey(x => x.ID);

            builder
                .Property(x => x.Name)
                .HasMaxLength(15)
                .IsRequired();

            builder
                .HasData(SeededData);
        }

        public ICollection<PostState> SeededData
        {
            get
            {
                return new List<PostState>
                {
                    new PostState
                    {
                        ID = PostStateConstants.DraftStateID,
                        Name = PostStateConstants.DraftStateName
                    },
                    new PostState
                    {
                        ID = PostStateConstants.PendingStateID,
                        Name = PostStateConstants.PendingStateName
                    },
                    new PostState
                    {
                        ID = PostStateConstants.ApprovedStateID,
                        Name = PostStateConstants.ApprovedStateName
                    },
                    new PostState
                    {
                        ID = PostStateConstants.RejectedStateID,
                        Name = PostStateConstants.RejectedStateName
                    }
                };
            }
        }
    }
}
