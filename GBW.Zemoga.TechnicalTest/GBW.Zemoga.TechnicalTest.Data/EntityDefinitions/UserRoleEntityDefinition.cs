﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GBW.Zemoga.TechnicalTest.Data.EntityDefinitions
{
    public class UserRoleEntityDefinition : IEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder
                .ToTable(nameof(UserRole));

            builder
                .HasKey(x => x.ID);

            builder
                .Property(x => x.Name)
                .HasMaxLength(15)
                .IsRequired();

            builder
                .HasData(SeededData);
        }

        public ICollection<UserRole> SeededData { 
            get
            {
                return new List<UserRole>
                {
                    new UserRole
                    {
                        ID = UserRolesConstants.PublicRoleID,
                        Name = UserRolesConstants.PublicRoleName
                    },
                    new UserRole
                    {
                        ID = UserRolesConstants.WriterRoleID,
                        Name = UserRolesConstants.WriterRoleName
                    },
                    new UserRole
                    {
                        ID = UserRolesConstants.EditorRoleID,
                        Name = UserRolesConstants.EditorRoleName
                    },
                    new UserRole
                    {
                        ID = UserRolesConstants.AdministratorRoleID,
                        Name = UserRolesConstants.AdministratorRoleName
                    }
                };
            } 
        }
    }
}
