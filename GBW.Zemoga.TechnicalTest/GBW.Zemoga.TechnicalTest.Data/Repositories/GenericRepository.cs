﻿using GBW.Zemoga.TechnicalTest.Data.UnitsOfWork.EntityFramework.Contexts;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities.Commons;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace GBW.Zemoga.TechnicalTest.Data.Repositories
{
    internal class GenericRepository<T> : IRepository<T> where T : class, IUniqueEntity, IAuditableEntity
    {
        private readonly MainDBContext _mainContext;
        private readonly UserInformation _userInformation;
        private readonly IQueryable<T> _dbSet;

        public GenericRepository(MainDBContext mainContext, UserInformation userInformation)
        {
            _mainContext = mainContext;
            _userInformation = userInformation;
            _dbSet = _mainContext.Set<T>();
        }

        private IQueryable<T> EagerLoadProperties(IQueryable<T> query, string propertiesToEagerLoad = "")
        {
            if (string.IsNullOrEmpty(propertiesToEagerLoad))
            {
                return query;
            }

            foreach (var propertyToLoad in propertiesToEagerLoad.Split(",", StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(propertyToLoad);
            }
            return query;
        }

        public async Task<T?> GetByIDAsync(Guid ID, string propertiesToEagerLoad = "")
        {
            var query = _dbSet;
            query = EagerLoadProperties(query, propertiesToEagerLoad);

            return await query.FirstOrDefaultAsync(x => x.ID == ID);
        }

        public async Task<ICollection<T>> GetListAsync(
            Expression<Func<T, bool>>? queryExpression = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderingFunction = null,
            string propertiesToEagerLoad = "",
            bool getDeletedOnly = false,
            int take = 0,
            int skip = 0)
        {
            var query = _dbSet;
            query = EagerLoadProperties(query, propertiesToEagerLoad);

            if (queryExpression != null)
            {
                query = query.Where(queryExpression);
            }

            if (!getDeletedOnly)
            {
                query = query.Where(x => 
                    x.DeleteTimestamp == null && 
                    x.DeleteUserID == null);
            }
            else
            {
                query = query.Where(x =>
                    x.DeleteTimestamp != null &&
                    x.DeleteUserID != null);
            }

            if (orderingFunction != null)
            {
                query = orderingFunction(query);
            }

            if (take > 0)
            {
                query = query.Take(take);
            }

            if (skip > 0)
            {
                query = query.Skip(skip);
            }

            return await query.ToListAsync();
        }


        public T Insert(T entity)
        {
            if (entity.ID == Guid.Empty)
            {
                entity.ID = Guid.NewGuid();
            }

            entity.CreateTimestamp = DateTime.Now;
            entity.CreateUserID = _userInformation.User?.ID ?? Guid.Empty;

            _mainContext.Add(entity); 
            
            return entity;
        }

        public T Update(T entity)
        {
            entity.UpdateTimestamp = DateTime.Now;
            entity.UpdateUserID = _userInformation.User?.ID ?? Guid.Empty;

            _mainContext.Update(entity);

            return entity;
        }

        public T Delete(T entity, bool hardDelete = false)
        {
            if (hardDelete)
            {
                _mainContext.Remove(entity);
            }
            else
            {
                entity.DeleteTimestamp = DateTime.Now;
                entity.DeleteUserID = _userInformation.User?.ID ?? Guid.Empty;
            }

            return entity;
        }
    }
}
