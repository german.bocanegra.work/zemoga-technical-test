﻿using GBW.Zemoga.TechnicalTest.Data.UnitsOfWork.EntityFramework.Contexts;
using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace GBW.Zemoga.TechnicalTest.Business.Tests.Helpers
{
    public class SqLiteDbContextBuilder
    {
        private SqliteConnection? _connection;
        private MainDBContext? _context;

        public MainDBContext Context
        {
            get
            {
                return _context ??= BuildTestContext();
            }
        }

        private MainDBContext BuildTestContext()
        {
            _connection = new SqliteConnection("Filename=:memory:");
            _connection.Open();
            var options = new DbContextOptionsBuilder<MainDBContext>()
                .UseSqlite(_connection)
                .EnableSensitiveDataLogging()
                .Options;

            var context = new MainDBContext(options);
            if (context.Database.EnsureCreated())
            {
                context.AddRange(Users);
                context.AddRange(Posts);

                context.SaveChanges();
            }

            return context;
        }

        private ICollection<User> Users = new List<User> {
            new User
            {
                ID = Guid.Parse("cf09d4d7-182a-4133-b604-cfac931aec69"),
                UserName = "test_user",
                DisplayName = "Test User",
                Password = "some_password",
                RoleID = UserRolesConstants.WriterRoleID,
                SortOrder = 1,
                IsActive = true
            }
        };

        private ICollection<Post> Posts = new List<Post> {
            new Post
            {
                ID = Guid.Parse("593c1c91-f2c3-4e3a-883f-e3439e1b8cf0"),
                SortOrder = 1,
                OwnerUserID = Guid.Parse("cf09d4d7-182a-4133-b604-cfac931aec69"),
                Title = "Title 1",
                Content = "Content 1",
                StatusID = PostStateConstants.ApprovedStateID,
                SubmitDate = DateTime.Now,
                PublishDate = DateTime.Now
            },
            new Post
            {
                ID = Guid.Parse("e4fec88f-8daa-45fc-8178-92701b390b6e"),
                SortOrder = 2,
                OwnerUserID = Guid.Parse("cf09d4d7-182a-4133-b604-cfac931aec69"),
                Title = "Title 2",
                Content = "Content 2",
                StatusID = PostStateConstants.ApprovedStateID,
                SubmitDate = DateTime.Now,
                PublishDate = DateTime.Now
            }
        };
    }
}
