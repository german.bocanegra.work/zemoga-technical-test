﻿using GBW.Zemoga.TechnicalTest.Business.Services;
using GBW.Zemoga.TechnicalTest.Business.Tests.Helpers;
using GBW.Zemoga.TechnicalTest.Data.UnitsOfWork;
using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;
using GBW.Zemoga.TechnicalTest.Domain.Validators;
using Moq;

namespace GBW.Zemoga.TechnicalTest.Business.Tests.Services
{
    public class PostServiceTests
    {
        private PostService? _service;

        private IUnitOfWork? _unitOfWork;
        private Mock<IPostServiceValidator>? _postValidator;

        public PostServiceTests()
        {
            InstantiateTestClasses();
        }

        private void InstantiateTestClasses()
        {
            var userInformation = new UserInformation
            {
                AccessTokenID = Guid.Parse("d57cc945-6161-4eff-a492-0a0caf37ce0a"),
                User = new User
                {
                    ID = Guid.Parse("cf09d4d7-182a-4133-b604-cfac931aec69"),
                    UserName = "test_user",
                    DisplayName = "Test User",
                    Password = "some_password",
                    RoleID = UserRolesConstants.WriterRoleID,
                    SortOrder = 1,
                    IsActive = true
                }
            };

            _unitOfWork = new MainUnitOfWork(new SqLiteDbContextBuilder().Context, userInformation);
            _postValidator = new Mock<IPostServiceValidator>();

            _service = new PostService(
                _unitOfWork,
                _postValidator.Object,
                userInformation);
        }

        [Fact]
        public async Task GetPostAsync_Found_ReturnsObjectAsync()
        {
            var postID = Guid.Parse("593c1c91-f2c3-4e3a-883f-e3439e1b8cf0");
            var result = await _service.GetPostAsync(postID);

            Assert.NotNull(result);
        }

        [Fact]
        public async Task GetPostAsync_NotFound_ThrowsServiceValidationExceptionAsync()
        {
            var threwExpectedException = false;

            try
            {
                var postID = Guid.Parse("593c1c91-f2c3-4e3a-883f-e3439e1b8cf2");
                var result = await _service.GetPostAsync(postID);
            }
            catch (EntityNotFoundException ex)
            {
                threwExpectedException = true;
                Assert.Equal("Entity not found", ex.Message);
            }

            Assert.True(threwExpectedException);
        }

        [Fact]
        public async Task GetPostAsync_EmptyGuid_ThrowsServiceValidationExceptionAsync()
        {
            var threwExpectedException = false;

            _postValidator
                .Setup(x => x.ValidateGetPostRequest(It.Is<Guid>(y => y == Guid.Empty)))
                .Throws(new ServiceValidationException("Mocked title", "Mocked message"));

            try
            {
                var postID = Guid.Empty;
                var result = await _service.GetPostAsync(postID);
            }
            catch (ServiceValidationException ex)
            {
                threwExpectedException = true;
                Assert.Equal("Service validation exception", ex.Message);
            }

            Assert.True(threwExpectedException);
        }
    }
}
