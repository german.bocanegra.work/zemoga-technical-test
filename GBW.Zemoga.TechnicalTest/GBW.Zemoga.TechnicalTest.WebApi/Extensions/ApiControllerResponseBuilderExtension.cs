﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses;
using Microsoft.AspNetCore.Mvc;

namespace GBW.Zemoga.TechnicalTest.WebApi.Extensions
{
    public static class ApiControllerResponseBuilderExtension
    {
        public static BaseResponse BuildSuccessfullBaseResponse(this ControllerBase controller, object data)
        {
            var result = new BaseResponse
            {
                Message = "Success",
                Data = data,
                Error = null
            };

            return result;
        }
    }
}
