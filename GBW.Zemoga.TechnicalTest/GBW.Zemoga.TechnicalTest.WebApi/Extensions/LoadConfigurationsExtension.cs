﻿namespace GBW.Zemoga.TechnicalTest.WebApi.Extensions
{
    public static class LoadConfigurationsExtension
    {
        /// <summary>
        /// Note: Configure Parameter Store access in this method if required as well as cache handling to reduce hits to AWS.
        /// Note: Extension method must be called in Program.cs to have effect.
        /// </summary>
        public static ConfigurationManager LoadExternalSourceMockFile(this ConfigurationManager configuration)
        {
            configuration.AddJsonFile("externalSourceMockSettings.json", false, true);

            return configuration;
        }
    }
}
