﻿using Microsoft.OpenApi.Models;

namespace GBW.Zemoga.TechnicalTest.WebApi.Extensions
{
    public static class SwaggerConfigurationExtension
    {
        /// <summary>
        /// Note: Extension method must be called in Program.cs to have effect.
        /// </summary>
        public static IServiceCollection ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Blog Engine .Net App",
                    Description = "Zemoga - Technical Test",
                    Contact = new OpenApiContact
                    {
                        Name = "Developed by German Bocanegra",
                        Url = new Uri("https://www.linkedin.com/in/german-leonardo-bocanegra-vasquez")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Licensed under MIT",
                        Url = new Uri("https://www.mit.edu/~amini/LICENSE.md")
                    }
                });
            });

            return services;
        }
    }
}
