﻿using GBW.Zemoga.TechnicalTest.Business.Services;
using GBW.Zemoga.TechnicalTest.Business.Validators;
using GBW.Zemoga.TechnicalTest.Data.UnitsOfWork;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.Services;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;
using GBW.Zemoga.TechnicalTest.Domain.Validators;

namespace GBW.Zemoga.TechnicalTest.WebApi.Extensions
{
    public static class DependenciesExtension
    {
        /// <summary>
        /// Note: Extension method must be called in Program.cs to have effect.
        /// </summary>
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<UserInformation>();
            services.AddScoped<IUnitOfWork, MainUnitOfWork>();

            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPostService, PostService>();
            services.AddScoped<IPostCommentService, PostCommentService>();
            services.AddScoped<IPostReviewService, PostReviewService>();

            services.AddScoped<IPostServiceValidator, PostServiceValidator>();
            services.AddScoped<IPostCommentServiceValidator, PostCommentServiceValidator>();
            services.AddScoped<IPostReviewServiceValidator, PostReviewServiceValidator>();
            services.AddScoped<IUserServiceValidator, UserServiceValidator>();

            return services;
        }
    }
}
