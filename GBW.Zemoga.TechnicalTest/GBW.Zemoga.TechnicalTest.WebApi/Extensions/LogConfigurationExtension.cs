﻿using Serilog;

namespace GBW.Zemoga.TechnicalTest.WebApi.Extensions
{
    public static class LogConfigurationExtension
    {
        /// <summary>
        /// Note: Extension method must be called in Program.cs to have effect.
        /// Note: Serilog uses static variables, this configuration is effectively a singleton instance.
        /// </summary>
        public static IServiceCollection ConfigureSerilog(this IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console(
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();

            Log.Debug("Logging as expected");

            return services;
        }
    }
}
