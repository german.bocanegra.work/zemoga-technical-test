﻿using GBW.Zemoga.TechnicalTest.Data.UnitsOfWork.EntityFramework.Contexts;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace GBW.Zemoga.TechnicalTest.WebApi.Extensions
{
    public static class EntityFrameworkDBExtension
    {
        public static IServiceCollection BuildMySqlDBContext(this IServiceCollection services, IConfiguration configuration)
        {
            // To do: move to a safe storage
            var password = Environment.GetEnvironmentVariable("DB_PASSCODE ") ?? configuration["ConnectionStrings:DefaultPassword"] ?? "";
            var connectionString = Environment.GetEnvironmentVariable("DB_CONNECTION_STRING") ?? configuration["ConnectionStrings:DefaultConnection"] ?? "";

            if (string.IsNullOrEmpty(connectionString) || string.IsNullOrEmpty(password))
            {
                throw new SystemException("Missing database connection data!");
            }

            connectionString = connectionString.Replace("Password=;", $"Password={password};");

            var version = new Version(8, 0, 32);
            var serverVersion = new MySqlServerVersion(version);

            var buildContextResult = services.AddDbContext<MainDBContext>(
                options =>
                {
                    options
                        .UseMySql(
                            connectionString,
                            serverVersion,
                            x =>
                            {
                                x.MigrationsAssembly(typeof(Program)
                                    .GetTypeInfo().Assembly.GetName().Name);
                            });
                }
            );

            return buildContextResult;
        }
    }
}
