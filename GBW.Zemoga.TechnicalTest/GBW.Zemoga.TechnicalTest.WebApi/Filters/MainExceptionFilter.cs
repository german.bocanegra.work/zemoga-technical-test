﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace GBW.Zemoga.TechnicalTest.WebApi.Filters
{
    public class MainExceptionFilter : IExceptionFilter
    {
        private readonly IHostEnvironment _hostEnvironment;

        public MainExceptionFilter(IHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
        }   

        public void OnException(ExceptionContext context)
        {
            if (context.Exception.GetType() == typeof(ServiceValidationException))
            {
                var validationException = (ServiceValidationException)context.Exception;
                var validationResult = new JsonResult(new BaseResponse
                {
                    Message = validationException.Message ?? "Service validation exception",
                    Data = null,
                    Error = validationException.Error
                });
                validationResult.StatusCode = StatusCodes.Status400BadRequest;
                context.Result = validationResult;
                return;
            }
            else if (context.Exception.GetType() == typeof(EntityNotFoundException))
            {
                var validationException = (EntityNotFoundException)context.Exception;
                var validationResult = new JsonResult(new BaseResponse
                {
                    Message = validationException.Message ?? "Entity not found",
                    Data = null,
                    Error = validationException.Error
                });
                validationResult.StatusCode = StatusCodes.Status404NotFound;
                context.Result = validationResult;
                return;
            }
            else if (context.Exception.GetType() == typeof(ServiceAuthorizationException))
            {
                var validationException = (ServiceAuthorizationException)context.Exception;
                var validationResult = new JsonResult(new BaseResponse
                {
                    Message = validationException.Message ?? "Action forbidden for current User",
                    Data = null,
                    Error = validationException.Error
                });
                validationResult.StatusCode = StatusCodes.Status403Forbidden;
                context.Result = validationResult;
                return;
            }
            else if (context.Exception.GetType() == typeof(ServiceAuthenticationException))
            {
                var validationException = (ServiceAuthenticationException)context.Exception;
                var validationResult = new JsonResult(new BaseResponse
                {
                    Message = validationException.Message ?? "User not authenticated",
                    Data = null,
                    Error = validationException.Error
                });
                validationResult.StatusCode = StatusCodes.Status401Unauthorized;
                context.Result = validationResult;
                return;
            }

            var result = new JsonResult(new BaseResponse
            {
                Message = "Internal server error, contact the administrator",
                Data = null,
                Error = _hostEnvironment.IsDevelopment() ? new Dictionary<string, string> { { "stackTrace", context.Exception.ToString() } } : null
            });
            result.StatusCode = StatusCodes.Status500InternalServerError;
            context.Result = result;
            return;
        }
    }
}
