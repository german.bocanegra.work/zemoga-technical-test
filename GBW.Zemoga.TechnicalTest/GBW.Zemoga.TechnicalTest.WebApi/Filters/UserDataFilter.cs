﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace GBW.Zemoga.TechnicalTest.WebApi.Filters
{
    public class UserDataFilter : IAsyncAuthorizationFilter
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserInformation _userInformation;

        public UserDataFilter(IConfiguration configuration, IUnitOfWork unitOfWork, UserInformation userInformation)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
            _userInformation = userInformation;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext filterContext)
        {
            var endpoint = filterContext.HttpContext.GetEndpoint();
            if (endpoint?.Metadata?.GetMetadata<IAllowAnonymous>() != null)
            {
                return;
            }

            var headers = filterContext.HttpContext.Request.Headers;

            if (headers == null || !headers.Any() || !headers.ContainsKey("Authorization"))
            {
                filterContext.Result = new UnauthorizedObjectResult(new BaseResponse
                {
                    Message = "Invalid bearer token",
                    Data = null,
                    Error = new Dictionary<string, string> { { "AccessToken", "The bearer token in the request is missing" } }
                });
                return;
            }

            var authHeader = headers["Authorization"];

            if (authHeader.IsNullOrEmpty() || !authHeader.First().ToString().StartsWith("Bearer "))
            {
                filterContext.Result = new UnauthorizedObjectResult(new BaseResponse
                {
                    Message = "Invalid bearer token",
                    Data = null,
                    Error = new Dictionary<string, string> { { "AccessToken", "The bearer token in the request is incomplete or invalid" } }
                });
                return;
            }

            var bearer = authHeader.First().Replace("Bearer ", "");

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration["Jwt:Key"]);

            try
            {
                tokenHandler.ValidateToken(bearer, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;


                var accessTokenID = Guid.Parse(jwtToken.Claims.First(x => x.Type == "AccessTokenID").Value);
                _userInformation.AccessTokenID = accessTokenID;

                var userID = Guid.Parse(jwtToken.Claims.First(x => x.Type == "UserID").Value);

                var user = await _unitOfWork.Users.GetByIDAsync(userID);
                _userInformation.User = user;
            }
            catch (ArgumentException ex)
            {
                filterContext.Result = new UnauthorizedObjectResult(new BaseResponse
                {
                    Message = "Bearer token is invalid",
                    Data = null,
                    Error = new Dictionary<string, string> { { "AccessToken", "The bearer token provided could not be read" } }
                });
                return;
            }
            catch (SecurityTokenExpiredException ex)
            {
                filterContext.Result = new UnauthorizedObjectResult(new BaseResponse
                {
                    Message = "Bearer token has expired",
                    Data = null,
                    Error = new Dictionary<string, string> { { "AccessToken", "JSON web token has run past it's lifetime" } }
                });
                return;
            }
        }
    }
}
