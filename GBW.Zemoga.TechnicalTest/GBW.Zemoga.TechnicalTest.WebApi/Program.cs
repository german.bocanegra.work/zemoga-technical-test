using GBW.Zemoga.TechnicalTest.WebApi.Extensions;
using GBW.Zemoga.TechnicalTest.WebApi.Filters;

namespace GBW.Zemoga.TechnicalTest.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddControllers(options =>
            {
                options.Filters.Add<MainExceptionFilter>();
                options.Filters.Add<UserDataFilter>();
            });


            builder.Configuration.LoadExternalSourceMockFile();
            builder.Services.BuildMySqlDBContext(builder.Configuration);

            builder.Services.ConfigureJwtAuthentication(builder.Configuration);
            builder.Services.AddAuthorization();

            builder.Services.RegisterServices();
            builder.Services.ConfigureSwagger();
            builder.Services.ConfigureSerilog();

            var app = builder.Build();

            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseAuthorization();

            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.MapControllers();
            app.Run();
        }
    }
}