﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GBW.Zemoga.TechnicalTest.WebApi.Migrations
{
    /// <inheritdoc />
    public partial class _20230228_added_default_user : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "AccessToken",
                type: "longtext",
                nullable: false)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "ID", "CreateTimestamp", "CreateUserID", "DeleteTimestamp", "DeleteUserID", "DisplayName", "IsActive", "Password", "RoleID", "SortOrder", "UpdateTimestamp", "UpdateUserID", "UserName" },
                values: new object[] { new Guid("83f4730a-3ed0-4693-b015-1d0abdf2f558"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), null, null, "Development Administrator", true, "56f862b6-133d-4a21-a3e0-41206ba325ea", new Guid("e6150285-f2d0-4ded-a12f-c6c173f99edb"), 1, null, null, "dev_admin" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "ID",
                keyValue: new Guid("83f4730a-3ed0-4693-b015-1d0abdf2f558"));

            migrationBuilder.DropColumn(
                name: "UserName",
                table: "AccessToken");
        }
    }
}
