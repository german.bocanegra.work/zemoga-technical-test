﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace GBW.Zemoga.TechnicalTest.WebApi.Migrations
{
    /// <inheritdoc />
    public partial class _20230228_added_seed_users_of_all_roles : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "ID", "CreateTimestamp", "CreateUserID", "DeleteTimestamp", "DeleteUserID", "DisplayName", "IsActive", "Password", "RoleID", "SortOrder", "UpdateTimestamp", "UpdateUserID", "UserName" },
                values: new object[,]
                {
                    { new Guid("23be9e88-62a2-48fe-acc0-3e9eb8294040"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), null, null, "Editor 1", true, "3", new Guid("3281a8b0-7559-4357-bebe-f5d4bd2381db"), 4, null, null, "editor_1" },
                    { new Guid("26981d11-59dd-4e76-95cb-cb8309f72e6a"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), null, null, "Editor 2", true, "4", new Guid("3281a8b0-7559-4357-bebe-f5d4bd2381db"), 5, null, null, "editor_2" },
                    { new Guid("3d06d0b7-6be2-4eff-8528-adccef660bed"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), null, null, "Writer 2", true, "2", new Guid("826f6672-9b73-4a43-94da-6f97ce6427f7"), 3, null, null, "writer_2" },
                    { new Guid("98771b2c-72a1-4461-beeb-f2c1ac67e1cf"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), null, null, "Public 1", true, "5", new Guid("c4f1ef0e-4cbb-4861-af41-a4d589e82891"), 6, null, null, "public_1" },
                    { new Guid("b5f846f6-fe27-4e1a-b787-2e658984c4f1"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), null, null, "Writer 1", true, "1", new Guid("826f6672-9b73-4a43-94da-6f97ce6427f7"), 2, null, null, "writer_1" },
                    { new Guid("de3c1355-960b-40e9-9374-cdc97c71806f"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), null, null, "Public 2", true, "6", new Guid("c4f1ef0e-4cbb-4861-af41-a4d589e82891"), 7, null, null, "public_2" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "ID",
                keyValue: new Guid("23be9e88-62a2-48fe-acc0-3e9eb8294040"));

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "ID",
                keyValue: new Guid("26981d11-59dd-4e76-95cb-cb8309f72e6a"));

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "ID",
                keyValue: new Guid("3d06d0b7-6be2-4eff-8528-adccef660bed"));

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "ID",
                keyValue: new Guid("98771b2c-72a1-4461-beeb-f2c1ac67e1cf"));

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "ID",
                keyValue: new Guid("b5f846f6-fe27-4e1a-b787-2e658984c4f1"));

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "ID",
                keyValue: new Guid("de3c1355-960b-40e9-9374-cdc97c71806f"));
        }
    }
}
