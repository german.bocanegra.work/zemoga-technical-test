﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GBW.Zemoga.TechnicalTest.WebApi.Migrations
{
    /// <inheritdoc />
    public partial class _20230227_added_administrator_role_type : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "ID", "CreateTimestamp", "CreateUserID", "DeleteTimestamp", "DeleteUserID", "Name", "UpdateTimestamp", "UpdateUserID" },
                values: new object[] { new Guid("e6150285-f2d0-4ded-a12f-c6c173f99edb"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), null, null, "Administrator", null, null });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumn: "ID",
                keyValue: new Guid("e6150285-f2d0-4ded-a12f-c6c173f99edb"));
        }
    }
}
