﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GBW.Zemoga.TechnicalTest.WebApi.Migrations
{
    /// <inheritdoc />
    public partial class _20230226_added_audit_properties_to_constants_tables : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreateTimestamp",
                table: "UserRole",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "CreateUserID",
                table: "UserRole",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<DateTime>(
                name: "DeleteTimestamp",
                table: "UserRole",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "DeleteUserID",
                table: "UserRole",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateTimestamp",
                table: "UserRole",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdateUserID",
                table: "UserRole",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateTimestamp",
                table: "PostState",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "CreateUserID",
                table: "PostState",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<DateTime>(
                name: "DeleteTimestamp",
                table: "PostState",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "DeleteUserID",
                table: "PostState",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateTimestamp",
                table: "PostState",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdateUserID",
                table: "PostState",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateTimestamp",
                table: "PostReviewAction",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "CreateUserID",
                table: "PostReviewAction",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<DateTime>(
                name: "DeleteTimestamp",
                table: "PostReviewAction",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "DeleteUserID",
                table: "PostReviewAction",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateTimestamp",
                table: "PostReviewAction",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdateUserID",
                table: "PostReviewAction",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.UpdateData(
                table: "PostReviewAction",
                keyColumn: "ID",
                keyValue: new Guid("37d69614-9cd3-4775-8083-b11cdcf70ced"),
                columns: new[] { "CreateTimestamp", "CreateUserID", "DeleteTimestamp", "DeleteUserID", "UpdateTimestamp", "UpdateUserID" },
                values: new object[] { DateTime.Now, new Guid("00000000-0000-0000-0000-000000000000"), null, null, null, null });

            migrationBuilder.UpdateData(
                table: "PostReviewAction",
                keyColumn: "ID",
                keyValue: new Guid("92588cf0-5644-45f2-ac4c-7ecfaa911677"),
                columns: new[] { "CreateTimestamp", "CreateUserID", "DeleteTimestamp", "DeleteUserID", "UpdateTimestamp", "UpdateUserID" },
                values: new object[] { DateTime.Now, new Guid("00000000-0000-0000-0000-000000000000"), null, null, null, null });

            migrationBuilder.UpdateData(
                table: "PostState",
                keyColumn: "ID",
                keyValue: new Guid("23498051-9fe4-4e14-a127-9673cde56ff1"),
                columns: new[] { "CreateTimestamp", "CreateUserID", "DeleteTimestamp", "DeleteUserID", "UpdateTimestamp", "UpdateUserID" },
                values: new object[] { DateTime.Now, new Guid("00000000-0000-0000-0000-000000000000"), null, null, null, null });

            migrationBuilder.UpdateData(
                table: "PostState",
                keyColumn: "ID",
                keyValue: new Guid("53c3f7f6-86f2-4488-87da-0d0de056c785"),
                columns: new[] { "CreateTimestamp", "CreateUserID", "DeleteTimestamp", "DeleteUserID", "UpdateTimestamp", "UpdateUserID" },
                values: new object[] { DateTime.Now, new Guid("00000000-0000-0000-0000-000000000000"), null, null, null, null });

            migrationBuilder.UpdateData(
                table: "PostState",
                keyColumn: "ID",
                keyValue: new Guid("d3585807-c805-4c53-a188-a8685990c82d"),
                columns: new[] { "CreateTimestamp", "CreateUserID", "DeleteTimestamp", "DeleteUserID", "UpdateTimestamp", "UpdateUserID" },
                values: new object[] { DateTime.Now, new Guid("00000000-0000-0000-0000-000000000000"), null, null, null, null });

            migrationBuilder.UpdateData(
                table: "PostState",
                keyColumn: "ID",
                keyValue: new Guid("dd1a641a-4835-40bb-9755-51fb1bf672aa"),
                columns: new[] { "CreateTimestamp", "CreateUserID", "DeleteTimestamp", "DeleteUserID", "UpdateTimestamp", "UpdateUserID" },
                values: new object[] { DateTime.Now, new Guid("00000000-0000-0000-0000-000000000000"), null, null, null, null });

            migrationBuilder.UpdateData(
                table: "UserRole",
                keyColumn: "ID",
                keyValue: new Guid("3281a8b0-7559-4357-bebe-f5d4bd2381db"),
                columns: new[] { "CreateTimestamp", "CreateUserID", "DeleteTimestamp", "DeleteUserID", "UpdateTimestamp", "UpdateUserID" },
                values: new object[] { DateTime.Now, new Guid("00000000-0000-0000-0000-000000000000"), null, null, null, null });

            migrationBuilder.UpdateData(
                table: "UserRole",
                keyColumn: "ID",
                keyValue: new Guid("826f6672-9b73-4a43-94da-6f97ce6427f7"),
                columns: new[] { "CreateTimestamp", "CreateUserID", "DeleteTimestamp", "DeleteUserID", "UpdateTimestamp", "UpdateUserID" },
                values: new object[] { DateTime.Now, new Guid("00000000-0000-0000-0000-000000000000"), null, null, null, null });

            migrationBuilder.UpdateData(
                table: "UserRole",
                keyColumn: "ID",
                keyValue: new Guid("c4f1ef0e-4cbb-4861-af41-a4d589e82891"),
                columns: new[] { "CreateTimestamp", "CreateUserID", "DeleteTimestamp", "DeleteUserID", "UpdateTimestamp", "UpdateUserID" },
                values: new object[] { DateTime.Now, new Guid("00000000-0000-0000-0000-000000000000"), null, null, null, null });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreateTimestamp",
                table: "UserRole");

            migrationBuilder.DropColumn(
                name: "CreateUserID",
                table: "UserRole");

            migrationBuilder.DropColumn(
                name: "DeleteTimestamp",
                table: "UserRole");

            migrationBuilder.DropColumn(
                name: "DeleteUserID",
                table: "UserRole");

            migrationBuilder.DropColumn(
                name: "UpdateTimestamp",
                table: "UserRole");

            migrationBuilder.DropColumn(
                name: "UpdateUserID",
                table: "UserRole");

            migrationBuilder.DropColumn(
                name: "CreateTimestamp",
                table: "PostState");

            migrationBuilder.DropColumn(
                name: "CreateUserID",
                table: "PostState");

            migrationBuilder.DropColumn(
                name: "DeleteTimestamp",
                table: "PostState");

            migrationBuilder.DropColumn(
                name: "DeleteUserID",
                table: "PostState");

            migrationBuilder.DropColumn(
                name: "UpdateTimestamp",
                table: "PostState");

            migrationBuilder.DropColumn(
                name: "UpdateUserID",
                table: "PostState");

            migrationBuilder.DropColumn(
                name: "CreateTimestamp",
                table: "PostReviewAction");

            migrationBuilder.DropColumn(
                name: "CreateUserID",
                table: "PostReviewAction");

            migrationBuilder.DropColumn(
                name: "DeleteTimestamp",
                table: "PostReviewAction");

            migrationBuilder.DropColumn(
                name: "DeleteUserID",
                table: "PostReviewAction");

            migrationBuilder.DropColumn(
                name: "UpdateTimestamp",
                table: "PostReviewAction");

            migrationBuilder.DropColumn(
                name: "UpdateUserID",
                table: "PostReviewAction");
        }
    }
}
