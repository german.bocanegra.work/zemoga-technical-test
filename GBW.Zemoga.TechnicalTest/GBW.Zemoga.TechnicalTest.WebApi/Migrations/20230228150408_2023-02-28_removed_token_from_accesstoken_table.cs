﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GBW.Zemoga.TechnicalTest.WebApi.Migrations
{
    /// <inheritdoc />
    public partial class _20230228_removed_token_from_accesstoken_table : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AccessToken_Token",
                table: "AccessToken");

            migrationBuilder.DropColumn(
                name: "Token",
                table: "AccessToken");

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                table: "AccessToken",
                type: "varchar(60)",
                maxLength: 60,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                table: "AccessToken",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(60)",
                oldMaxLength: 60)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "Token",
                table: "AccessToken",
                type: "varchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_AccessToken_Token",
                table: "AccessToken",
                column: "Token");
        }
    }
}
