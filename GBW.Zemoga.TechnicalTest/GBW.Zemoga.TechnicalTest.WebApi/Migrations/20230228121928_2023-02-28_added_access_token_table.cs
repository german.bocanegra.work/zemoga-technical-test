﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GBW.Zemoga.TechnicalTest.WebApi.Migrations
{
    /// <inheritdoc />
    public partial class _20230228_added_access_token_table : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccessToken",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    CreateUserID = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    UpdateUserID = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    DeleteUserID = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    CreateTimestamp = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    UpdateTimestamp = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UserID = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    Token = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    LastRefreshTimestamp = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    TokenDurationInMinutes = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessToken", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AccessToken_User_UserID",
                        column: x => x.UserID,
                        principalTable: "User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_Post_OwnerUserID",
                table: "Post",
                column: "OwnerUserID");

            migrationBuilder.CreateIndex(
                name: "IX_AccessToken_Token",
                table: "AccessToken",
                column: "Token");

            migrationBuilder.CreateIndex(
                name: "IX_AccessToken_UserID",
                table: "AccessToken",
                column: "UserID");

            migrationBuilder.AddForeignKey(
                name: "FK_Post_User_OwnerUserID",
                table: "Post",
                column: "OwnerUserID",
                principalTable: "User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Post_User_OwnerUserID",
                table: "Post");

            migrationBuilder.DropTable(
                name: "AccessToken");

            migrationBuilder.DropIndex(
                name: "IX_Post_OwnerUserID",
                table: "Post");
        }
    }
}
