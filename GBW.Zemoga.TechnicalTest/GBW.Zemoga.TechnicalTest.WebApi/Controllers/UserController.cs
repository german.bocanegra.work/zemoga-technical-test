﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Users;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.Services;
using GBW.Zemoga.TechnicalTest.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GBW.Zemoga.TechnicalTest.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly UserInformation _userInformation;

        public UserController(IUserService userService, UserInformation userInformation)
        {
            _userService = userService;
            _userInformation = userInformation;
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _userService.GetUserAsync(id);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ListAsync([FromQuery] Guid? roleID, [FromQuery] bool? isActive)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _userService.ListUsersAsync(new ListUsersRequest
            {
                RoleID = roleID,
                IsActive = isActive
            });

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] CreateUserRequest request)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _userService.CreateUserAsync(request);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PutAsync(Guid id, [FromBody] UpdateUserRequest request)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            request.ID = id;
            var result = await _userService.UpdateUserAsync(request);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _userService.DeleteUserAsync(id);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }
    }
}
