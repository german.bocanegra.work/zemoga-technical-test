﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Auth;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.Services;
using GBW.Zemoga.TechnicalTest.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace GBW.Zemoga.TechnicalTest.WebApi.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [AllowAnonymous]
        [HttpGet("healthcheck")]
        public IActionResult HealthCheck()
        {
            return Ok(this.BuildSuccessfullBaseResponse("OK"));
        }

        [AllowAnonymous]
        [HttpPost("CreateAccessToken")]
        public async Task<IActionResult> PostAsync([FromBody] AuthenticateUserRequest request)
        {
            var result = await _authenticationService.CreateAccessToken(request.UserName, request.UserPassword);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpGet("RefreshAccessToken")]
        public async Task<IActionResult> GetAsync()
        {
            var headers = Request.Headers;
            var authHeader = headers["Authorization"];

            if (StringValues.IsNullOrEmpty(authHeader) || authHeader.FirstOrDefault() == null || !authHeader.First().ToString().Contains("Bearer "))
            {
                throw new ServiceAuthenticationException("Invalid bearer token");
            }

            var bearer = authHeader.First().Replace("Bearer ", "");
            var result = await _authenticationService.RefreshAccessToken(bearer);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }
    }
}
