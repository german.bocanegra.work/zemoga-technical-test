﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Posts;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.Services;
using GBW.Zemoga.TechnicalTest.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GBW.Zemoga.TechnicalTest.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;
        private readonly UserInformation _userInformation;

        public PostController(IPostService postService, UserInformation userInformation)
        {
            _postService = postService;
            _userInformation = userInformation;
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var result = await _postService.GetPostAsync(id);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ListAsync([FromQuery] Guid? postOwnerID, [FromQuery] Guid? postStatusID)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _postService.ListPostsAsync(new ListPostsRequest
            {
                PostOwnerID = postOwnerID,
                PostStatusID = postStatusID
            });

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }


        [Authorize]
        [HttpGet("published")]
        public async Task<IActionResult> ListPublishedAsync()
        {
            var result = await _postService.ListPostsAsync(new ListPostsRequest
            {
                PostOwnerID = null,
                PostStatusID = PostStateConstants.ApprovedStateID
            });

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpGet("owner")]
        public async Task<IActionResult> ListOwnerAsync([FromQuery] Guid? postStatusID)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.WriterRoleID && _userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _postService.ListPostsAsync(new ListPostsRequest
            {
                PostOwnerID = _userInformation.User.ID,
                PostStatusID = postStatusID
            });

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpGet("pending")]
        public async Task<IActionResult> ListPendingAsync()
        {
            if (_userInformation.User.RoleID != UserRolesConstants.EditorRoleID && _userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _postService.ListPostsAsync(new ListPostsRequest
            {
                PostOwnerID = null,
                PostStatusID = PostStateConstants.PendingStateID
            });

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }


        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] CreatePostRequest request)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.WriterRoleID && _userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _postService.CreatePostAsync(request);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PutAsync(Guid id, [FromBody] UpdatePostRequest request)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.WriterRoleID && _userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            request.ID = id;
            var result = await _postService.UpdatePostAsync(request);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpPatch("submit/{id}")]
        public async Task<IActionResult> SubmitAsync(Guid id)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.WriterRoleID && _userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _postService.SubmitPostAsync(id);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.WriterRoleID && _userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _postService.DeletePostAsync(id);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }
    }
}
