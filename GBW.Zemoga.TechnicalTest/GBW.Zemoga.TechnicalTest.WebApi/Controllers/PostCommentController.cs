﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostComments;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.Services;
using GBW.Zemoga.TechnicalTest.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GBW.Zemoga.TechnicalTest.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostCommentController : ControllerBase
    {
        private readonly IPostCommentService _postCommentService;
        private readonly UserInformation _userInformation;

        public PostCommentController(IPostCommentService postCommentService, UserInformation userInformation)
        {
            _postCommentService = postCommentService;
            _userInformation = userInformation;
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _postCommentService.GetPostCommentAsync(id);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ListAsync([FromQuery] Guid? postID, [FromQuery] Guid? commenterID)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _postCommentService.ListPostCommentsAsync(new ListPostCommentRequest
            {
                PostID = postID,
                CommenterID = commenterID
            });

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] CreatePostCommentRequest request)
        {
            var result = await _postCommentService.CreatePostCommentAsync(request);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PutAsync(Guid id, [FromBody] UpdatePostCommentRequest request)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            request.ID = id;
            var result = await _postCommentService.UpdatePostCommentAsync(request);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _postCommentService.DeletePostCommentAsync(id);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }
    }
}
