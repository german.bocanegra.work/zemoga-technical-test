﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostReviews;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.Services;
using GBW.Zemoga.TechnicalTest.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GBW.Zemoga.TechnicalTest.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostReviewController : ControllerBase
    {
        private readonly IPostReviewService _postReviewService;
        private readonly UserInformation _userInformation;

        public PostReviewController(IPostReviewService postReviewService, UserInformation userInformation)
        {
            _postReviewService = postReviewService;
            _userInformation = userInformation;
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _postReviewService.GetPostReviewAsync(id);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ListAsync([FromQuery] Guid? postID, [FromQuery] Guid? reviewerID, [FromQuery] Guid? actionID)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _postReviewService.ListPostReviewsAsync(new ListPostReviewsRequest
            {
                PostID = postID,
                ReviewerID = reviewerID,
                ActionID = actionID
            });

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> ReviewAsync([FromBody] ReviewPostRequest request)
        {
            if (_userInformation.User.RoleID != UserRolesConstants.EditorRoleID && _userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var result = await _postReviewService.ReviewPostAsync(request);

            return Ok(this.BuildSuccessfullBaseResponse(result));
        }
    }
}
