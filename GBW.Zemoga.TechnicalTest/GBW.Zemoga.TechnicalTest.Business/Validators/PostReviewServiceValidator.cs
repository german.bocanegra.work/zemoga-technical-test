﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.Constants.Enumerators;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostReviews;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;
using GBW.Zemoga.TechnicalTest.Domain.Validators;

namespace GBW.Zemoga.TechnicalTest.Business.Validators
{
    public class PostReviewServiceValidator : IPostReviewServiceValidator
    {
        private readonly IUnitOfWork _unitOfWork;

        public PostReviewServiceValidator(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void ValidateGetPostReviewRequest(Guid ID)
        {
            if (ID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(PostReview.ID), "PostReview ID cannot be an empty Guid");
            }

            return;
        }

        public async Task ValidateListPostReviewRequestAsync(ListPostReviewsRequest request)
        {
            if (request.PostID != null && request.PostID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(ListPostReviewsRequest.PostID), "PostID cannot be an empty Guid");
            }

            if (request.ReviewerID != null && request.ReviewerID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(ListPostReviewsRequest.ReviewerID), "ReviewerID cannot be an empty Guid");
            }

            if (request.ActionID != null && request.ActionID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(ListPostReviewsRequest.ActionID), "ActionID cannot be an empty Guid");
            }

            if (request.PostID != null)
            {
                var post = await _unitOfWork.Posts.GetByIDAsync(request.PostID.Value);
                if (post == null)
                {
                    throw new ServiceValidationException(nameof(ListPostReviewsRequest.PostID), "Post was not found");
                }
            }

            if (request.ReviewerID != null)
            {
                var reviewer = await _unitOfWork.Users.GetByIDAsync(request.ReviewerID.Value);
                if (reviewer == null)
                {
                    throw new ServiceValidationException(nameof(ListPostReviewsRequest.ReviewerID), "Reviewer User was not found");
                }
            }

            if (request.ActionID != null)
            {
                if (request.ActionID.Value != PostReviewActionConstants.ApprovePostActionID &&
                    request.ActionID.Value != PostReviewActionConstants.RejectPostActionID)
                {
                    throw new ServiceValidationException(nameof(ListPostReviewsRequest.ActionID), "ActionID does not belong to a valid review action");
                }
            }

            return;
        }

        public void ValidateReviewPostRequest(ReviewPostRequest request, Post post)
        {
            if (request.PostID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(ReviewPostRequest.PostID), "PostID cannot be an empty Guid");
            }

            if (request.Action == ReviewPostActionsEnum.Reject && string.IsNullOrEmpty(request.ReviewComment))
            {
                throw new ServiceValidationException(nameof(ReviewPostRequest.ReviewComment), "ReviewComment cannot be empty when rejecting a post");
            }

            if (post.StatusID != PostStateConstants.PendingStateID)
            {
                throw new ServiceValidationException(nameof(ReviewPostRequest.PostID), "Post must be submitted to be able to review it");
            }

            return;
        }
    }
}
