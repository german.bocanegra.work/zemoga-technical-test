﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Posts;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Users;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;
using GBW.Zemoga.TechnicalTest.Domain.Validators;

namespace GBW.Zemoga.TechnicalTest.Business.Validators
{
    public class UserServiceValidator : IUserServiceValidator
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserServiceValidator(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void ValidateGetUserRequest(Guid ID)
        {
            if (ID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(User.ID), "User ID cannot be an empty Guid");
            }

            return;
        }

        public void ValidateListUserRequest(ListUsersRequest request)
        {
            if (request.RoleID != null && request.RoleID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(ListUsersRequest.RoleID), "RoleID cannot be an empty Guid");
            }

            if (request.RoleID != null)
            {
                if (request.RoleID.Value != UserRolesConstants.PublicRoleID &&
                    request.RoleID.Value != UserRolesConstants.WriterRoleID &&
                    request.RoleID.Value != UserRolesConstants.EditorRoleID &&
                    request.RoleID.Value != UserRolesConstants.AdministratorRoleID)
                {
                    throw new ServiceValidationException(nameof(ListUsersRequest.RoleID), "RoleID does not belong to a valid role");
                }
            }

            return;
        }

        public void ValidateCreateUserRequest(CreateUserRequest request)
        {
            if (request.UserName != null && request.UserName == string.Empty)
            {
                throw new ServiceValidationException(nameof(CreateUserRequest.UserName), "UserName cannot be empty");
            }

            if (request.DisplayName != null && request.DisplayName == string.Empty)
            {
                throw new ServiceValidationException(nameof(CreateUserRequest.DisplayName), "DisplayName cannot be empty");
            }

            if (request.Password != null && request.Password == string.Empty)
            {
                throw new ServiceValidationException(nameof(CreateUserRequest.Password), "Password cannot be empty");
            }

            if (request.RoleID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(CreateUserRequest.RoleID), "RoleID cannot be an empty Guid");
            }

            if (request.RoleID != UserRolesConstants.PublicRoleID &&
                request.RoleID != UserRolesConstants.WriterRoleID &&
                request.RoleID != UserRolesConstants.EditorRoleID &&
                request.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceValidationException(nameof(CreateUserRequest.RoleID), "RoleID does not belong to a valid role");
            }

            if (request.SortOrder != null && request.SortOrder <= 0)
            {
                throw new ServiceValidationException(nameof(CreateUserRequest.SortOrder), "SortOrder cannot be lower than 1");
            }

            return;
        }

        public void ValidateUpdateUserRequest(UpdateUserRequest request)
        {
            if (request.ID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(UpdateUserRequest.ID), "User ID cannot be an empty Guid");
            }

            if (request.UserName != null && request.UserName == string.Empty)
            {
                throw new ServiceValidationException(nameof(UpdateUserRequest.UserName), "UserName cannot be empty");
            }

            if (request.DisplayName != null && request.DisplayName == string.Empty)
            {
                throw new ServiceValidationException(nameof(UpdateUserRequest.DisplayName), "DisplayName cannot be empty");
            }

            if (request.Password != null && request.Password == string.Empty)
            {
                throw new ServiceValidationException(nameof(UpdateUserRequest.Password), "Password cannot be empty");
            }

            if (request.RoleID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(UpdateUserRequest.RoleID), "RoleID cannot be an empty Guid");
            }

            if (request.RoleID != UserRolesConstants.PublicRoleID &&
                request.RoleID != UserRolesConstants.WriterRoleID &&
                request.RoleID != UserRolesConstants.EditorRoleID &&
                request.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceValidationException(nameof(UpdateUserRequest.RoleID), "RoleID does not belong to a valid role");
            }

            if (request.SortOrder != null && request.SortOrder <= 0)
            {
                throw new ServiceValidationException(nameof(UpdateUserRequest.SortOrder), "SortOrder cannot be lower than 1");
            }

            return;
        }

        public void ValidateDeleteUserRequest(Guid ID)
        {
            if (ID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(Post.ID), "User ID cannot be an empty Guid");
            }

            return;
        }
    }
}
