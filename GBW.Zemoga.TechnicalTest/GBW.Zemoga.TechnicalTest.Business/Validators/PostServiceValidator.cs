﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Posts;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;
using GBW.Zemoga.TechnicalTest.Domain.Validators;

namespace GBW.Zemoga.TechnicalTest.Business.Validators
{
    public class PostServiceValidator : IPostServiceValidator
    {
        private readonly IUnitOfWork _unitOfWork;

        public PostServiceValidator(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void ValidateGetPostRequest(Guid ID)
        {
            if (ID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(Post.ID), "Post ID cannot be an empty Guid");
            }

            return;
        }

        public async Task ValidateListPostRequestAsync(ListPostsRequest request)
        {
            if (request.PostOwnerID != null && request.PostOwnerID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(ListPostsRequest.PostOwnerID), "PostOwnerID cannot be an empty Guid");
            }

            if (request.PostStatusID != null && request.PostStatusID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(ListPostsRequest.PostStatusID), "PostStatusID cannot be an empty Guid");
            }

            if (request.PostOwnerID != null)
            {
                var postOwner = await _unitOfWork.Users.GetByIDAsync(request.PostOwnerID.Value);
                if (postOwner == null)
                {
                    throw new ServiceValidationException(nameof(ListPostsRequest.PostOwnerID), "PostOwner was not found");
                }
            }

            if (request.PostStatusID != null)
            {
                if (request.PostStatusID.Value != PostStateConstants.ApprovedStateID && 
                    request.PostStatusID.Value != PostStateConstants.RejectedStateID &&
                    request.PostStatusID.Value != PostStateConstants.DraftStateID &&
                    request.PostStatusID.Value != PostStateConstants.PendingStateID)
                {
                    throw new ServiceValidationException(nameof(ListPostsRequest.PostStatusID), "PostStatusID does not belong to a valid state");
                }
            }

            return;
        }

        public void ValidateCreatePostRequest(CreatePostRequest request)
        {
            if (request.Title != null && request.Title == string.Empty)
            {
                throw new ServiceValidationException(nameof(CreatePostRequest.Title), "Title cannot be empty");
            }

            if (request.Content != null && request.Content == string.Empty)
            {
                throw new ServiceValidationException(nameof(CreatePostRequest.Content), "Content cannot be empty");
            }

            if (request.SortOrder != null && request.SortOrder <= 0)
            {
                throw new ServiceValidationException(nameof(CreatePostRequest.SortOrder), "SortOrder cannot be lower than 1");
            }

            return;
        }

        public void ValidateUpdatePostRequest(UpdatePostRequest request)
        {
            if (request.ID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(UpdatePostRequest.ID), "Post ID cannot be an empty Guid");
            }

            if (request.Title != null && request.Title == string.Empty)
            {
                throw new ServiceValidationException(nameof(UpdatePostRequest.Title), "Title cannot be empty");
            }

            if (request.Content != null && request.Content == string.Empty)
            {
                throw new ServiceValidationException(nameof(UpdatePostRequest.Content), "Content cannot be empty");
            }

            if (request.SortOrder != null && request.SortOrder <= 0)
            {
                throw new ServiceValidationException(nameof(UpdatePostRequest.SortOrder), "SortOrder cannot be lower than 1");
            }

            return;
        }

        public void ValidateDeletePostRequest(Guid ID)
        {
            if (ID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(Post.ID), "Post ID cannot be an empty Guid");
            }

            return;
        }
    }
}
