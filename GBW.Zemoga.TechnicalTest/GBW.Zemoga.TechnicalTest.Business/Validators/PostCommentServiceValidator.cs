﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostComments;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;
using GBW.Zemoga.TechnicalTest.Domain.Validators;

namespace GBW.Zemoga.TechnicalTest.Business.Validators
{
    public class PostCommentServiceValidator : IPostCommentServiceValidator
    {
        private readonly IUnitOfWork _unitOfWork;

        public PostCommentServiceValidator(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void ValidateGetPostCommentRequest(Guid ID)
        {
            if (ID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(PostComment.ID), "Post Comment ID cannot be an empty Guid");
            }

            return;
        }

        public async Task ValidateListPostCommentRequestAsync(ListPostCommentRequest request)
        {
            if (request.PostID != null && request.PostID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(ListPostCommentRequest.PostID), "PostID cannot be an empty Guid");
            }

            if (request.CommenterID != null && request.CommenterID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(ListPostCommentRequest.CommenterID), "CommenterID cannot be an empty Guid");
            }

            if (request.PostID != null)
            {
                var post = await _unitOfWork.Posts.GetByIDAsync(request.PostID.Value);
                if (post == null)
                {
                    throw new ServiceValidationException(nameof(ListPostCommentRequest.PostID), "Post was not found");
                }
            }

            if (request.CommenterID != null)
            {
                var commenter = await _unitOfWork.Users.GetByIDAsync(request.CommenterID.Value);
                if (commenter == null)
                {
                    throw new ServiceValidationException(nameof(ListPostCommentRequest.CommenterID), "Commenter was not found");
                }
            }

            return;
        }

        public async Task ValidateCreatePostCommentRequestAsync(CreatePostCommentRequest request)
        {
            if (request.PostID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(CreatePostCommentRequest.PostID), "PostID cannot be an empty Guid");
            }

            if (request.Title != null && request.Title == string.Empty)
            {
                throw new ServiceValidationException(nameof(CreatePostCommentRequest.Title), "Title cannot be empty");
            }

            if (request.Content != null && request.Content == string.Empty)
            {
                throw new ServiceValidationException(nameof(CreatePostCommentRequest.Content), "Content cannot be empty");
            }

            if (request.SortOrder != null && request.SortOrder <= 0)
            {
                throw new ServiceValidationException(nameof(CreatePostCommentRequest.SortOrder), "SortOrder cannot be lower than 1");
            }

            var post = await _unitOfWork.Posts.GetByIDAsync(request.PostID);
            if (post == null)
            {
                throw new ServiceValidationException(nameof(CreatePostCommentRequest.PostID), "Post was not found");
            }
            if (post.StatusID != PostStateConstants.ApprovedStateID)
            {
                throw new ServiceValidationException(nameof(CreatePostCommentRequest.PostID), "Post must be published to add comments to it");
            }

            return;
        }

        public void ValidateUpdatePostCommentRequest(UpdatePostCommentRequest request)
        {
            if (request.ID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(UpdatePostCommentRequest.ID), "Post Comment ID cannot be an empty Guid");
            }

            if (request.Title != null && request.Title == string.Empty)
            {
                throw new ServiceValidationException(nameof(UpdatePostCommentRequest.Title), "Title cannot be empty");
            }

            if (request.Content != null && request.Content == string.Empty)
            {
                throw new ServiceValidationException(nameof(UpdatePostCommentRequest.Content), "Content cannot be empty");
            }

            if (request.SortOrder != null && request.SortOrder <= 0)
            {
                throw new ServiceValidationException(nameof(UpdatePostCommentRequest.SortOrder), "SortOrder cannot be lower than 1");
            }

            return;
        }

        public void ValidateDeletePostCommentRequest(Guid ID)
        {
            if (ID == Guid.Empty)
            {
                throw new ServiceValidationException(nameof(PostComment.ID), "Post Comment ID cannot be an empty Guid");
            }

            return;
        }
    }
}
