﻿using GBW.Zemoga.TechnicalTest.Business.Validators;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostComments;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.PostComments;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.Mappers.PostComments;
using GBW.Zemoga.TechnicalTest.Domain.Services;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;
using GBW.Zemoga.TechnicalTest.Domain.Validators;

namespace GBW.Zemoga.TechnicalTest.Business.Services
{
    public class PostCommentService : IPostCommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPostCommentServiceValidator _postCommentServiceValidator;
        private readonly UserInformation _userInformation;

        public PostCommentService(IUnitOfWork unitOfWork, IPostCommentServiceValidator postCommentServiceValidator, UserInformation userInformation)
        {
            _unitOfWork = unitOfWork;
            _postCommentServiceValidator = postCommentServiceValidator;
            _userInformation = userInformation;
        }

        public async Task<PostCommentResponse> GetPostCommentAsync(Guid ID)
        {
            _postCommentServiceValidator.ValidateGetPostCommentRequest(ID);

            var postComment = await _unitOfWork.PostComments.GetByIDAsync(
                ID, 
                propertiesToEagerLoad: $"{nameof(PostComment.Commenter)}");

            if (postComment == null)
            {
                throw new EntityNotFoundException(nameof(PostComment), ID.ToString());
            }

            var postCommentResponse = PostCommentMapper.Map(postComment);
            return postCommentResponse;
        }

        public async Task<ICollection<PostCommentResponse>> ListPostCommentsAsync(ListPostCommentRequest request)
        {
            await _postCommentServiceValidator.ValidateListPostCommentRequestAsync(request);

            var postComments = await _unitOfWork.PostComments.GetListAsync(
                x =>
                    request.PostID != null ? x.PostID == request.PostID : true &&
                    request.CommenterID != null ? x.CommenterID == request.CommenterID : true,
                o => o.OrderBy(y => y.SortOrder),
                propertiesToEagerLoad: $"{nameof(PostComment.Commenter)}"
                );

            if (postComments == null || !postComments.Any())
            {
                return new List<PostCommentResponse>();
            }

            var postCommentResponses = new List<PostCommentResponse>();
            foreach (var postComment in postComments)
            {
                postCommentResponses.Add(PostCommentMapper.Map(postComment));
            }

            return postCommentResponses;
        }

        public async Task<PostCommentResponse> CreatePostCommentAsync(CreatePostCommentRequest request)
        {
            await _postCommentServiceValidator.ValidateCreatePostCommentRequestAsync(request);

            var postComment = new PostComment
            {
                ID = Guid.NewGuid(),
                SortOrder = request.SortOrder != null ? request.SortOrder.Value : 0,

                PostID = request.PostID,
                CommenterID = _userInformation.User?.ID ?? Guid.Empty,
                Title = request.Title,
                Content = request.Content
            };

            var postCommentResult = _unitOfWork.PostComments.Insert(postComment);
            await _unitOfWork.SaveChangesAsync();

            var postCommentResponse = PostCommentMapper.Map(postCommentResult);
            return postCommentResponse;
        }

        public async Task<PostCommentResponse> UpdatePostCommentAsync(UpdatePostCommentRequest request)
        {
            _postCommentServiceValidator.ValidateUpdatePostCommentRequest(request);

            var postComment = await _unitOfWork.PostComments.GetByIDAsync(request.ID);

            if (postComment == null)
            {
                throw new EntityNotFoundException(nameof(PostComment), request.ID.ToString());
            }

            if (!string.IsNullOrEmpty(request.Title))
            {
                postComment.Title = request.Title;
            }

            if (!string.IsNullOrEmpty(request.Content))
            {
                postComment.Content = request.Content;
            }

            if (request.SortOrder != null && request.SortOrder > 0)
            {
                // To do: properly implement reordering with sort order service
                postComment.SortOrder = request.SortOrder.Value;
            }

            var postCommentResult = _unitOfWork.PostComments.Update(postComment);
            await _unitOfWork.SaveChangesAsync();

            var postCommentResponse = PostCommentMapper.Map(postCommentResult);
            return postCommentResponse;
        }

        public async Task<PostCommentResponse> DeletePostCommentAsync(Guid ID)
        {
            _postCommentServiceValidator.ValidateDeletePostCommentRequest(ID);

            var postComment = await _unitOfWork.PostComments.GetByIDAsync(ID);

            if (postComment == null)
            {
                throw new EntityNotFoundException(nameof(PostComment), ID.ToString());
            }

            var postCommentResult = _unitOfWork.PostComments.Delete(postComment);
            await _unitOfWork.SaveChangesAsync();

            var postCommentResponse = PostCommentMapper.Map(postCommentResult);
            return postCommentResponse;
        }
    }
}
