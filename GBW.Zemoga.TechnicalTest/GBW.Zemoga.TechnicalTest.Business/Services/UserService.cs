﻿using GBW.Zemoga.TechnicalTest.Business.Validators;
using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Users;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.Users;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.Mappers.Users;
using GBW.Zemoga.TechnicalTest.Domain.Services;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;
using GBW.Zemoga.TechnicalTest.Domain.Validators;

namespace GBW.Zemoga.TechnicalTest.Business.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserServiceValidator _userServiceValidator;
        private readonly UserInformation _userInformation;

        public UserService(IUnitOfWork unitOfWork, IUserServiceValidator userServiceValidator, UserInformation userInformation)
        {
            _unitOfWork = unitOfWork;
            _userServiceValidator = userServiceValidator;
            _userInformation = userInformation;
        }

        private void ValidateUserIsAdministrator()
        {
            if (_userInformation.User == null || _userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceAuthorizationException(nameof(User));
            }
        }

        public async Task<UserResponse> GetUserAsync(Guid ID)
        {
            ValidateUserIsAdministrator();
            _userServiceValidator.ValidateGetUserRequest(ID);

            var user = await _unitOfWork.Users.GetByIDAsync(ID);

            if (user == null)
            {
                throw new EntityNotFoundException(nameof(User), ID.ToString());
            }

            var userResponse = UserMapper.Map(user);
            return userResponse;
        }

        public async Task<ICollection<UserResponse>> ListUsersAsync(ListUsersRequest request)
        {
            ValidateUserIsAdministrator();
            _userServiceValidator.ValidateListUserRequest(request);

            var users = await _unitOfWork.Users.GetListAsync(
                null,
                o => o.OrderBy(y => y.SortOrder)
                );

            if (users == null || !users.Any())
            {
                return new List<UserResponse>();
            }

            var userResponses = new List<UserResponse>();
            foreach (var user in users)
            {
                userResponses.Add(UserMapper.Map(user));
            }

            return userResponses;
        }

        public async Task<UserResponse> CreateUserAsync(CreateUserRequest request)
        {
            ValidateUserIsAdministrator();
            _userServiceValidator.ValidateCreateUserRequest(request);

            if (request.RoleID != UserRolesConstants.EditorRoleID &&
                request.RoleID != UserRolesConstants.WriterRoleID &&
                request.RoleID != UserRolesConstants.PublicRoleID &&
                request.RoleID != UserRolesConstants.AdministratorRoleID)
            {
                throw new ServiceValidationException(nameof(CreateUserRequest.RoleID), "User RoleID must be a valid role ID");
            }

            var user = new User
            {
                ID = Guid.NewGuid(),
                SortOrder = request.SortOrder != null ? request.SortOrder.Value : 0,

                UserName = request.UserName,
                DisplayName = request.DisplayName,
                Password = request.Password,
                RoleID = request.RoleID,
                IsActive = request.IsActive
            };

            var userResult = _unitOfWork.Users.Insert(user);
            await _unitOfWork.SaveChangesAsync();

            var userResponse = UserMapper.Map(userResult);
            return userResponse;
        }

        public async Task<UserResponse> UpdateUserAsync(UpdateUserRequest request)
        {
            ValidateUserIsAdministrator();
            _userServiceValidator.ValidateUpdateUserRequest(request);

            var user = await _unitOfWork.Users.GetByIDAsync(request.ID);

            if (user == null)
            {
                throw new EntityNotFoundException(nameof(User), request.ID.ToString());
            }

            if (!string.IsNullOrEmpty(request.UserName))
            {
                user.UserName = request.UserName;
            }

            if (!string.IsNullOrEmpty(request.DisplayName))
            {
                user.DisplayName = request.DisplayName;
            }

            if (!string.IsNullOrEmpty(request.Password))
            {
                user.Password = request.Password;
            }

            if (request.RoleID != null)
            {
                if (request.RoleID != UserRolesConstants.EditorRoleID &&
                    request.RoleID != UserRolesConstants.WriterRoleID &&
                    request.RoleID != UserRolesConstants.PublicRoleID &&
                    request.RoleID != UserRolesConstants.AdministratorRoleID)
                {
                    throw new ServiceValidationException(nameof(CreateUserRequest.RoleID), "User RoleID must be a valid role ID");
                }

                user.RoleID = request.RoleID.Value;
            }

            if (request.IsActive != null)
            {
                user.IsActive = request.IsActive.Value;
            }

            if (request.SortOrder != null && request.SortOrder > 0)
            {
                // To do: properly implement reordering with sort order service
                user.SortOrder = request.SortOrder.Value;
            }

            var userResult = _unitOfWork.Users.Update(user);
            await _unitOfWork.SaveChangesAsync();

            var userResponse = UserMapper.Map(userResult);
            return userResponse;
        }

        public async Task<UserResponse> DeleteUserAsync(Guid ID)
        {
            ValidateUserIsAdministrator();
            _userServiceValidator.ValidateDeleteUserRequest(ID);

            var user = await _unitOfWork.Users.GetByIDAsync(ID);

            if (user == null)
            {
                throw new EntityNotFoundException(nameof(User), ID.ToString());
            }

            var userResult = _unitOfWork.Users.Delete(user);
            await _unitOfWork.SaveChangesAsync();

            var userResponse = UserMapper.Map(userResult);
            return userResponse;
        }
    }
}
