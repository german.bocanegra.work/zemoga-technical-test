﻿using GBW.Zemoga.TechnicalTest.Business.Validators;
using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.Constants.Enumerators;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostComments;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostReviews;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.PostReviews;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.Mappers.PostReviews;
using GBW.Zemoga.TechnicalTest.Domain.Services;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;
using GBW.Zemoga.TechnicalTest.Domain.Validators;

namespace GBW.Zemoga.TechnicalTest.Business.Services
{
    public class PostReviewService : IPostReviewService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPostReviewServiceValidator _postReviewServiceValidator;
        private readonly UserInformation _userInformation;

        public PostReviewService(IUnitOfWork unitOfWork, IPostReviewServiceValidator postReviewServiceValidator, UserInformation userInformation)
        {
            _unitOfWork = unitOfWork;
            _postReviewServiceValidator = postReviewServiceValidator;
            _userInformation = userInformation;
        }

        public async Task<PostReviewResponse> GetPostReviewAsync(Guid ID)
        {
            _postReviewServiceValidator.ValidateGetPostReviewRequest(ID);

            var postReview = await _unitOfWork.PostReviews.GetByIDAsync(
                ID,
                propertiesToEagerLoad: $"{nameof(PostReview.Reviewer)}");

            if (postReview == null)
            {
                throw new EntityNotFoundException(nameof(PostReview), ID.ToString());
            }

            var postReviewResponse = PostReviewMapper.Map(postReview);
            return postReviewResponse;
        }

        public async Task<ICollection<PostReviewResponse>> ListPostReviewsAsync(ListPostReviewsRequest request)
        {
            await _postReviewServiceValidator.ValidateListPostReviewRequestAsync(request);

            var postReviews = await _unitOfWork.PostReviews.GetListAsync(
                x =>
                    request.PostID != null ? x.PostID == request.PostID : true &&
                    request.ReviewerID != null ? x.ReviewerID == request.ReviewerID : true &&
                    request.ActionID != null ? x.ActionID == request.ActionID : true,
                o => o.OrderBy(y => y.SortOrder),
                propertiesToEagerLoad: $"{nameof(PostReview.Reviewer)}"
                );

            if (postReviews == null || !postReviews.Any())
            {
                return new List<PostReviewResponse>();
            }

            var postReviewResponses = new List<PostReviewResponse>();
            foreach (var postReview in postReviews)
            {
                postReviewResponses.Add(PostReviewMapper.Map(postReview));
            }

            return postReviewResponses;
        }

        public async Task<PostReviewResponse> ReviewPostAsync(ReviewPostRequest request)
        {
            var post = await _unitOfWork.Posts.GetByIDAsync(request.PostID);
            if (post == null)
            {
                throw new EntityNotFoundException(nameof(Post), request.PostID.ToString());
            }

            _postReviewServiceValidator.ValidateReviewPostRequest(request, post);

            var actionID = 
                    request.Action == ReviewPostActionsEnum.Approve ? 
                    PostReviewActionConstants.ApprovePostActionID : 
                    PostReviewActionConstants.RejectPostActionID;

            var postReview = new PostReview
            {
                ID = Guid.NewGuid(),

                PostID = request.PostID,
                ReviewerID = _userInformation.User?.ID ?? Guid.Empty,
                ActionID = actionID,
                RejectionComment = request.ReviewComment
            };
            
            if (request.Action == ReviewPostActionsEnum.Approve)
            {
                post = ApprovePost(post);
            }
            else
            {
                post = RejectPost(post);
            }

            var postReviewResult = _unitOfWork.PostReviews.Insert(postReview);
            var postResult = _unitOfWork.Posts.Update(post);

            await _unitOfWork.SaveChangesAsync();

            var postReviewResponse = PostReviewMapper.Map(postReviewResult);
            return postReviewResponse;
        }

        private Post ApprovePost(Post post)
        {
            post.StatusID = PostStateConstants.ApprovedStateID;
            post.PublishDate = DateTime.Now;

            return post;
        }

        private Post RejectPost(Post post)
        {
            post.StatusID = PostStateConstants.RejectedStateID;

            return post;
        }
    }
}
