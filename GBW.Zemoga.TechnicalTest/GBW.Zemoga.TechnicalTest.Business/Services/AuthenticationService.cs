﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.Services;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace GBW.Zemoga.TechnicalTest.Business.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserInformation _userInformation;

        private readonly string _issuerConfig;
        private readonly string _audienceConfig;
        private readonly string _keyConfig;
        private readonly int _accessTokenLifetimeInMinutesConfig;

        public AuthenticationService(IConfiguration configuration, IUnitOfWork unitOfWork, UserInformation userInformation)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
            _userInformation = userInformation;

            var issuerConfig = _configuration["Jwt:Issuer"];
            var audienceConfig = _configuration["Jwt:Audience"];
            var keyConfig = _configuration["Jwt:Key"];
            var accessTokenLifetimeInMinutesConfig = _configuration["Jwt:AccessTokenLifetimeInMinutes"];

            if (string.IsNullOrEmpty(issuerConfig) ||
                string.IsNullOrEmpty(audienceConfig) ||
                string.IsNullOrEmpty(keyConfig) ||
                string.IsNullOrEmpty(accessTokenLifetimeInMinutesConfig))
            {
                throw new ApplicationException("Missing authentication settings");
            }

            var parseResult = int.TryParse(accessTokenLifetimeInMinutesConfig, out var accessTokenLifetimeInMinutes);
            if (!parseResult)
            {
                throw new ApplicationException("Invalid value for 'Jwt:AccessTokenLifetimeInMinutes' configuration");
            }

            _issuerConfig = issuerConfig;
            _audienceConfig = audienceConfig;
            _keyConfig = keyConfig;
            _accessTokenLifetimeInMinutesConfig = accessTokenLifetimeInMinutes;
        }

        private string BuildJwtToken(Guid accessTokenID, Guid userID, string userName)
        {
            var issuer = _issuerConfig;
            var audience = _audienceConfig;
            var key = Encoding.ASCII.GetBytes(_keyConfig);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                new Claim("AccessTokenID", accessTokenID.ToString()),
                new Claim("UserID", userID.ToString()),
                new Claim("UserName", userName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
             }),
                Expires = DateTime.UtcNow.AddMinutes(_accessTokenLifetimeInMinutesConfig),
                Issuer = issuer,
                Audience = audience,
                SigningCredentials = new SigningCredentials
                (new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha512Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            var stringToken = tokenHandler.WriteToken(token);

            return stringToken;
        }

        public async Task<string> CreateAccessToken(string userName, string userPassword)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(userPassword))
            {
                throw new ServiceAuthenticationException(userName);
            }

            var userResult = await _unitOfWork.Users.GetListAsync(x => x.UserName.Equals(userName));
            if (userResult == null || !userResult.Any())
            {
                throw new ServiceAuthenticationException(userName);
            }

            var user = userResult.FirstOrDefault();
            if (user == null)
            {
                throw new ServiceAuthenticationException(userName);
            }

            if (!userPassword.Equals(user.Password))
            {
                throw new ServiceAuthenticationException(userName);
            }

            var accessToken = new AccessToken
            {
                ID = Guid.NewGuid(),
                UserID = user.ID,
                UserName = user.UserName,
                LastRefreshTimestamp = DateTime.Now,
                TokenDurationInMinutes = _accessTokenLifetimeInMinutesConfig
            };

            var bearerToken = BuildJwtToken(accessToken.ID, user.ID, userName);

            _unitOfWork.AccessTokens.Insert(accessToken);
            await _unitOfWork.SaveChangesAsync();

            return bearerToken;
        }

        public async Task<string> RefreshAccessToken(string currentBearerToken)
        {
            var accessTokenID = _userInformation.AccessTokenID;

            var accessToken = await _unitOfWork.AccessTokens.GetByIDAsync(accessTokenID);
            if (accessToken == null)
            {
                throw new ServiceAuthenticationException("Access token not found");
            }
            var bearerToken = BuildJwtToken(accessToken.ID, accessToken.UserID, accessToken.UserName);

            accessToken.LastRefreshTimestamp = DateTime.Now;
            _unitOfWork.AccessTokens.Update(accessToken);
            await _unitOfWork.SaveChangesAsync();

            return bearerToken;
        }
    }
}
