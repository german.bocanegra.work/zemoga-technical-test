﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostComments;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostReviews;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Posts;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.Posts;
using GBW.Zemoga.TechnicalTest.Domain.Exceptions;
using GBW.Zemoga.TechnicalTest.Domain.Mappers.Posts;
using GBW.Zemoga.TechnicalTest.Domain.Services;
using GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork;
using GBW.Zemoga.TechnicalTest.Domain.Validators;

namespace GBW.Zemoga.TechnicalTest.Business.Services
{
    public class PostService : IPostService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPostServiceValidator _postServiceValidator;
        private readonly UserInformation _userInformation;

        public PostService(IUnitOfWork unitOfWork, IPostServiceValidator postServiceValidator, UserInformation userInformation)
        {
            _unitOfWork = unitOfWork;
            _postServiceValidator = postServiceValidator;
            _userInformation = userInformation;
        }

        public async Task<PostResponse> GetPostAsync(Guid ID)
        {
            _postServiceValidator.ValidateGetPostRequest(ID);

            var post = await _unitOfWork.Posts.GetByIDAsync(
                ID, 
                propertiesToEagerLoad: $"{nameof(Post.Comments)},{nameof(Post.Reviews)},{nameof(Post.OwnerUser)}");

            if (post == null)
            {
                throw new EntityNotFoundException(nameof(Post), ID.ToString());
            }

            var postResponse = PostMapper.Map(post, mapReviews: post.OwnerUserID == _userInformation.User.ID);
            return postResponse;
        }

        public async Task<ICollection<PostResponse>> ListPostsAsync(ListPostsRequest request)
        {
            await _postServiceValidator.ValidateListPostRequestAsync(request);

            var posts = await _unitOfWork.Posts.GetListAsync(
                x =>
                    request.PostOwnerID != null ? x.OwnerUserID == request.PostOwnerID : true &&
                    request.PostStatusID != null ? x.StatusID == request.PostStatusID : true,
                o => o.OrderBy(y => y.SortOrder),
                propertiesToEagerLoad: $"{nameof(Post.Comments)},{nameof(Post.Reviews)},{nameof(Post.OwnerUser)}"
                );

            if (posts == null || !posts.Any())
            {
                return new List<PostResponse>();
            }

            var postResponses = new List<PostResponse>();
            foreach (var post in posts)
            {
                postResponses.Add(PostMapper.Map(post, mapReviews: post.OwnerUserID == _userInformation.User.ID));
            }

            return postResponses;
        }

        public async Task<PostResponse> CreatePostAsync(CreatePostRequest request)
        {
            _postServiceValidator.ValidateCreatePostRequest(request);

            var post = new Post
            {
                ID = Guid.NewGuid(),
                SortOrder = request.SortOrder != null ? request.SortOrder.Value : 0,

                OwnerUserID = _userInformation.User?.ID ?? Guid.Empty,
                Title = request.Title,
                Content = request.Content,
                StatusID = PostStateConstants.DraftStateID
            };

            var postResult = _unitOfWork.Posts.Insert(post);
            await _unitOfWork.SaveChangesAsync();

            var postResponse = PostMapper.Map(postResult);
            return postResponse;
        }

        public async Task<PostResponse> UpdatePostAsync(UpdatePostRequest request)
        {
            _postServiceValidator.ValidateUpdatePostRequest(request);

            var post = await _unitOfWork.Posts.GetByIDAsync(request.ID);
            if (post == null)
            {
                throw new EntityNotFoundException(nameof(Post), request.ID.ToString());
            }

            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID &&
                _userInformation.User.ID != post.OwnerUserID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            if (post.StatusID != PostStateConstants.DraftStateID && post.StatusID != PostStateConstants.RejectedStateID)
            {
                throw new ServiceValidationException(nameof(CreatePostCommentRequest.PostID), "Post must be in draft or rejected state to be able to modify it");
            }

            if (!string.IsNullOrEmpty(request.Title))
            {
                post.Title = request.Title;
            }

            if (!string.IsNullOrEmpty(request.Content))
            {
                post.Content = request.Content;
            }

            if (request.SortOrder != null && request.SortOrder > 0)
            {
                // To do: properly implement reordering with sort order service
                post.SortOrder = request.SortOrder.Value;
            }

            var postResult = _unitOfWork.Posts.Update(post);
            await _unitOfWork.SaveChangesAsync();

            var postResponse = PostMapper.Map(postResult);
            return postResponse;
        }

        public async Task<PostResponse> DeletePostAsync(Guid ID)
        {
            _postServiceValidator.ValidateDeletePostRequest(ID);

            var post = await _unitOfWork.Posts.GetByIDAsync(ID);
            if (post == null)
            {
                throw new EntityNotFoundException(nameof(Post), ID.ToString());
            }

            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID &&
                _userInformation.User.ID != post.OwnerUserID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            var postResult = _unitOfWork.Posts.Delete(post);
            await _unitOfWork.SaveChangesAsync();

            var postResponse = PostMapper.Map(postResult);
            return postResponse;
        }

        public async Task<PostResponse> SubmitPostAsync(Guid ID)
        {
            _postServiceValidator.ValidateDeletePostRequest(ID);

            var post = await _unitOfWork.Posts.GetByIDAsync(ID);
            if (post == null)
            {
                throw new EntityNotFoundException(nameof(Post), ID.ToString());
            }

            if (_userInformation.User.RoleID != UserRolesConstants.AdministratorRoleID &&
                _userInformation.User.ID != post.OwnerUserID)
            {
                throw new ServiceAuthorizationException(nameof(Post));
            }

            if (post.StatusID != PostStateConstants.DraftStateID && post.StatusID != PostStateConstants.RejectedStateID)
            {
                throw new ServiceValidationException(nameof(ReviewPostRequest.PostID), "Post must be in draft or rejected state to be able to publish it");
            }

            post.StatusID = PostStateConstants.PendingStateID;
            post.SubmitDate = DateTime.Now;

            var postResult = _unitOfWork.Posts.Update(post);
            await _unitOfWork.SaveChangesAsync();

            var postResponse = PostMapper.Map(postResult);
            return postResponse;
        }
    }
}
