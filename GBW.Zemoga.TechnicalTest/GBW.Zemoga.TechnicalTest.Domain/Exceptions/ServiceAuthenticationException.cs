﻿namespace GBW.Zemoga.TechnicalTest.Domain.Exceptions
{
    public class ServiceAuthenticationException : Exception
    {
        public readonly Dictionary<string, string> Error;

        public ServiceAuthenticationException(string userID) : base("User not authenticated")
        {
            Error = new Dictionary<string, string> { { "UserID", $"User with ID: {userID} is not authenticated" } };
        }
    }
}
