﻿namespace GBW.Zemoga.TechnicalTest.Domain.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public readonly Dictionary<string, string> Error;

        public EntityNotFoundException(string entityName, string entityID) : base("Entity not found")
        {
            Error = new Dictionary<string, string> { { entityName, $"{entityName} entity entry with ID: {entityID} was not found" } };
        }
    }
}
