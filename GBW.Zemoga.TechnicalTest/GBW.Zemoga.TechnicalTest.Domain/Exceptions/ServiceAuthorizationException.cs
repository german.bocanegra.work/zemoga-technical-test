﻿namespace GBW.Zemoga.TechnicalTest.Domain.Exceptions
{
    public class ServiceAuthorizationException : Exception
    {
        public readonly Dictionary<string, string> Error;

        public ServiceAuthorizationException(string entityName) : base("Action forbidden for current User")
        {
            Error = new Dictionary<string, string> { { entityName, $"The current user does not have the appropriate permissions for this action." } };
        }
    }
}
