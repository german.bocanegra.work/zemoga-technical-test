﻿namespace GBW.Zemoga.TechnicalTest.Domain.Exceptions
{
    public class ServiceValidationException : Exception
    {
        public readonly Dictionary<string, string> Error;

        public ServiceValidationException(string errorTitle, string errorMessage) : base("Service validation exception")
        {
            Error = new Dictionary<string, string> { { errorTitle, errorMessage } };
        }

        public ServiceValidationException(string exceptionMessage, string errorTitle, string errorMessage)
            : base(exceptionMessage)
        {
            Error = new Dictionary<string, string>{ { errorTitle, errorMessage } };
        }

        public ServiceValidationException(string exceptionMessage, Exception inner, string errorTitle, string errorMessage)
            : base(exceptionMessage, inner)
        {
            Error = new Dictionary<string, string>{ { errorTitle, errorMessage } };
        }
    }
}
