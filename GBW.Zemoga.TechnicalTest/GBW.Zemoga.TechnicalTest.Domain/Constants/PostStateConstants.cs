﻿namespace GBW.Zemoga.TechnicalTest.Domain.Constants
{
    public class PostStateConstants
    {
        public static readonly Guid DraftStateID = new Guid("23498051-9fe4-4e14-a127-9673cde56ff1");
        public static readonly Guid PendingStateID = new Guid("dd1a641a-4835-40bb-9755-51fb1bf672aa");
        public static readonly Guid ApprovedStateID = new Guid("53c3f7f6-86f2-4488-87da-0d0de056c785");
        public static readonly Guid RejectedStateID = new Guid("d3585807-c805-4c53-a188-a8685990c82d");

        public static readonly string DraftStateName = "Draft";
        public static readonly string PendingStateName = "Pending";
        public static readonly string ApprovedStateName = "Approved";
        public static readonly string RejectedStateName = "Rejected";

        public static string GetStatusNameByID(Guid ID)
        {
            if (ID == DraftStateID)
            {
                return DraftStateName;
            }
            else if (ID == PendingStateID)
            {
                return PendingStateName;
            }
            else if (ID == ApprovedStateID)
            {
                return ApprovedStateName;
            }
            else if (ID == RejectedStateID)
            {
                return RejectedStateName;
            }
            else
            {
                return "";
            }
        }
    }
}
