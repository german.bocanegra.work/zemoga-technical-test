﻿namespace GBW.Zemoga.TechnicalTest.Domain.Constants
{
    public class PostReviewActionConstants
    {
        public static readonly Guid ApprovePostActionID = new Guid("92588cf0-5644-45f2-ac4c-7ecfaa911677");
        public static readonly Guid RejectPostActionID = new Guid("37d69614-9cd3-4775-8083-b11cdcf70ced");

        public static readonly string ApprovePostActionName = "Approve";
        public static readonly string RejectPostActionName = "Reject";

        public static string GetActionNameByID(Guid ID)
        {
            if (ID == ApprovePostActionID)
            {
                return ApprovePostActionName;
            }
            else if (ID == RejectPostActionID)
            {
                return RejectPostActionName;
            }
            else
            {
                return "";
            }
        }
    }
}
