﻿namespace GBW.Zemoga.TechnicalTest.Domain.Constants
{
    public class UserRolesConstants
    {
        public static readonly Guid PublicRoleID = new Guid("c4f1ef0e-4cbb-4861-af41-a4d589e82891");
        public static readonly Guid WriterRoleID = new Guid("826f6672-9b73-4a43-94da-6f97ce6427f7");
        public static readonly Guid EditorRoleID = new Guid("3281a8b0-7559-4357-bebe-f5d4bd2381db");
        public static readonly Guid AdministratorRoleID = new Guid("e6150285-f2d0-4ded-a12f-c6c173f99edb");

        public static readonly string PublicRoleName = "Public";
        public static readonly string WriterRoleName = "Writer";
        public static readonly string EditorRoleName = "Editor";
        public static readonly string AdministratorRoleName = "Administrator";

        public static string GetRoleNameByID(Guid ID)
        {
            if (ID == PublicRoleID)
            {
                return PublicRoleName;
            }
            else if (ID == WriterRoleID)
            {
                return WriterRoleName;
            }
            else if (ID == EditorRoleID)
            {
                return EditorRoleName;
            }
            else if (ID == AdministratorRoleID)
            {
                return AdministratorRoleName;
            }
            else
            {
                return "";
            }
        }
    }
}
