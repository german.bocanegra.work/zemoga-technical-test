﻿namespace GBW.Zemoga.TechnicalTest.Domain.Constants.Enumerators
{
    public enum ReviewPostActionsEnum
    {
        Approve,
        Reject
    }
}
