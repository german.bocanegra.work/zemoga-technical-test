﻿using GBW.Zemoga.TechnicalTest.Domain.Constants.Enumerators;

namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostReviews
{
    public class ReviewPostRequest
    {
        public Guid PostID { get; set; }
        public ReviewPostActionsEnum Action { get; set; }
        public string? ReviewComment { get; set; }

        public ReviewPostRequest()
        {

        }
    }
}
