﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostReviews
{
    public class ListPostReviewsRequest
    {
        public Guid? PostID { get; set; }
        public Guid? ReviewerID { get; set; }
        public Guid? ActionID { get; set; }
    }
}
