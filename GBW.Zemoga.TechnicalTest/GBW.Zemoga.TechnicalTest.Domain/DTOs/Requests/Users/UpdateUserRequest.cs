﻿using System.Text.Json.Serialization;

namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Users
{
    public class UpdateUserRequest
    {
        [JsonIgnore]
        public Guid ID { get; set; }
        public int? SortOrder { get; set; }

        public string? UserName { get; set; }
        public string? DisplayName { get; set; }
        public string? Password { get; set; }
        public Guid? RoleID { get; set; }
        public bool? IsActive { get; set; }

        public UpdateUserRequest()
        {

        }
    }
}
