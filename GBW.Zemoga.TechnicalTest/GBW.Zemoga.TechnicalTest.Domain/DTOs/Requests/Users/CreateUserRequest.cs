﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Users
{
    public class CreateUserRequest
    {
        public int? SortOrder { get; set; }

        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public Guid RoleID { get; set; }
        public bool IsActive { get; set; }

        public CreateUserRequest()
        {
            UserName = string.Empty;
            DisplayName = string.Empty;
            Password = string.Empty;
            IsActive = true;
        }
    }
}
