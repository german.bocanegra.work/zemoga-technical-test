﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Users
{
    public class ListUsersRequest
    {
        public Guid? RoleID { get; set; }
        public bool? IsActive { get; set; }
    }
}
