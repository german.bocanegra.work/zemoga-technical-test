﻿using System.Text.Json.Serialization;

namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostComments
{
    public class UpdatePostCommentRequest
    {
        [JsonIgnore]
        public Guid ID { get; set; }
        public string? Title { get; set; }
        public string? Content { get; set; }
        public int? SortOrder { get; set; }

        public UpdatePostCommentRequest()
        {

        }
    }
}
