﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostComments
{
    public class CreatePostCommentRequest
    {
        public Guid PostID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int? SortOrder { get; set; }

        public CreatePostCommentRequest()
        {
            Title = string.Empty;
            Content = string.Empty;
        }
    }
}
