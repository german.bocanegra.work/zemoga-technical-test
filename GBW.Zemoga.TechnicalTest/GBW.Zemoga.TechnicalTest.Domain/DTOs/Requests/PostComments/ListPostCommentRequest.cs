﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostComments
{
    public class ListPostCommentRequest
    {
        public Guid? PostID { get; set; }
        public Guid? CommenterID { get; set; }
    }
}
