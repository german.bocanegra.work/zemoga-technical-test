﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Auth
{
    public class AuthenticateUserRequest
    {
        public string UserName { get; set; }
        public string UserPassword { get; set; }

        public AuthenticateUserRequest()
        {
            UserName = string.Empty;
            UserPassword = string.Empty;
        }
    }
}
