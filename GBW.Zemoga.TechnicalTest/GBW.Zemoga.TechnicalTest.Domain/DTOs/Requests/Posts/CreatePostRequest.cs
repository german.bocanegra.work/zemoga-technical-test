﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Posts
{
    public class CreatePostRequest
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int? SortOrder { get; set; }

        public CreatePostRequest()
        {
            Title = string.Empty; 
            Content = string.Empty;
        }
    }
}
