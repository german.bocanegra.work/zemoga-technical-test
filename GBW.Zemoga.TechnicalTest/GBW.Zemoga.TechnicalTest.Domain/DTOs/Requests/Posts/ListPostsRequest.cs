﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Posts
{
    public class ListPostsRequest
    {
        public Guid? PostOwnerID { get; set; }
        public Guid? PostStatusID { get; set; }

        public ListPostsRequest()
        {

        }
    }
}
