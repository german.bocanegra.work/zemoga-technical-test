﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.Users
{
    public class UserResponse
    {
        public Guid ID { get; set; }
        public Guid CreateUserID { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public DateTime? DeleteTimestamp { get; set; }

        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public Guid RoleID { get; set; }
        public string? RoleName { get; set; }
        public bool IsActive { get; set; }

        public int SortOrder { get; set; }

        public UserResponse()
        {
            UserName = string.Empty;
            DisplayName = string.Empty;
        }
    }
}
