﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.PostComments;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.PostReviews;

namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.Posts
{
    public class PostResponse
    {
        public Guid ID { get; set; }
        public Guid CreateUserID { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public DateTime? DeleteTimestamp { get; set; }

        public Guid OwnerUserID { get; set; }
        public string? OwnerUserName { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public Guid StatusID { get; set; }
        public string? StatusName { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? PublishDate { get; set; }

        public int SortOrder { get; set; }

        public ICollection<PostReviewResponse> Reviews { get; set; }
        public ICollection<PostCommentResponse> Comments { get; set; }

        public PostResponse()
        {
            Title = string.Empty;
            Content = string.Empty;

            Reviews = new List<PostReviewResponse>();
            Comments = new List<PostCommentResponse>();
        }
    }
}
