﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.PostComments
{
    public class PostCommentResponse
    {
        public Guid ID { get; set; }
        public Guid CreateUserID { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public DateTime? DeleteTimestamp { get; set; }

        public Guid CommenterID { get; set; }
        public string? CommenterUserName { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public PostCommentResponse()
        {
            Title = string.Empty;
            Content = string.Empty;
        }
    }
}
