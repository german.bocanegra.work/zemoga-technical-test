﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.PostReviews
{
    public class PostReviewResponse
    {
        public Guid ID { get; set; }
        public Guid CreateUserID { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public DateTime? DeleteTimestamp { get; set; }

        public Guid PostID { get; set; }
        public Guid ReviewerID { get; set; }
        public string? ReviewerUserName { get; set; }
        public Guid ActionID { get; set; }
        public string? ActionName { get; set; }
        public string? RejectionComment { get; set; }

        public PostReviewResponse()
        {

        }
    }
}
