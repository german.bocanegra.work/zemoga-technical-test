﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses
{
    public class BaseResponse
    {
        public string? Message { get; set; }
        public object? Data { get; set; }
        public object? Error { get; set; }
    }
}
