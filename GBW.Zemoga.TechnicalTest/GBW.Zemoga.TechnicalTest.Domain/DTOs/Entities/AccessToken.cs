﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities.Commons;

namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities
{
    public class AccessToken : IUniqueEntity, IAuditableEntity
    {
        public Guid ID { get; set; }
        public Guid CreateUserID { get; set; }
        public Guid? UpdateUserID { get; set; }
        public Guid? DeleteUserID { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public DateTime? UpdateTimestamp { get; set; }
        public DateTime? DeleteTimestamp { get; set; }

        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public DateTime LastRefreshTimestamp { get; set; }
        public int TokenDurationInMinutes { get; set; }

        public User? User { get; set; }

        public AccessToken()
        {
            UserName = string.Empty;
        }
    }
}
