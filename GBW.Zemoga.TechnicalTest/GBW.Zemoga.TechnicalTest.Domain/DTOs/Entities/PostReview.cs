﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities.Commons;

namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities
{
    public class PostReview : IUniqueEntity, IAuditableEntity, ISortableEntity
    {
        public Guid ID { get; set; }
        public Guid CreateUserID { get; set; }
        public Guid? UpdateUserID { get; set; }
        public Guid? DeleteUserID { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public DateTime? UpdateTimestamp { get; set; }
        public DateTime? DeleteTimestamp { get; set; }
        public int SortOrder { get; set; }

        public Guid PostID { get; set; }
        public Guid ReviewerID { get; set; }
        public Guid ActionID { get; set; }
        public string? RejectionComment { get; set; }

        public Post? Post { get; set; }
        public User? Reviewer { get; set; }
        public PostReviewAction? Action { get; set; }

        public PostReview(
            Guid ID, Guid CreateUserID, DateTime CreateTimestamp, int SortOrder, 
            Guid PostID, Guid ReviewerID, Guid ActionID, string? RejectionComment = null)
        {
            this.ID = ID;
            this.CreateUserID = CreateUserID;
            this.CreateTimestamp = CreateTimestamp;
            this.SortOrder = SortOrder;
            this.PostID = PostID;
            this.ReviewerID = ReviewerID;
            this.ActionID = ActionID;
            this.RejectionComment = RejectionComment;
        }
        public PostReview()
        {

        }
    }
}
