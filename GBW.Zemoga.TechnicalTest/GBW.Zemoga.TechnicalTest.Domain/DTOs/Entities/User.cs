﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities.Commons;

namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities
{
    public class User : IUniqueEntity, IAuditableEntity, ISortableEntity
    {
        public Guid ID { get; set; }
        public Guid CreateUserID { get; set; }
        public Guid? UpdateUserID { get; set; }
        public Guid? DeleteUserID { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public DateTime? UpdateTimestamp { get; set; }
        public DateTime? DeleteTimestamp { get; set; }
        public int SortOrder { get; set; }

        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public Guid RoleID { get; set; }
        public bool IsActive { get; set; }

        public UserRole? Role { get; set; }

        public User(
            Guid ID, Guid CreateUserID, DateTime CreateTimestamp, int SortOrder,
            string UserName, string DisplayName, string Password, Guid RoleID, bool IsActive = true)
        {
            this.ID = ID;
            this.CreateUserID = CreateUserID;
            this.CreateTimestamp = CreateTimestamp;
            this.SortOrder = SortOrder;
            this.UserName = UserName;
            this.DisplayName = DisplayName;
            this.Password = Password;
            this.RoleID = RoleID;
            this.IsActive = IsActive;
        }

        public User()
        {
            UserName = string.Empty;
            DisplayName = string.Empty;
            Password = string.Empty;
            IsActive = true;
        }
    }
}
