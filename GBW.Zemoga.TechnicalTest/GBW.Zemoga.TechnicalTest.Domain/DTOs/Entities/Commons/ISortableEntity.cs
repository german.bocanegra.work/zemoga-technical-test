﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities.Commons
{
    public interface ISortableEntity
    {
        int SortOrder { get; set; }
    }
}
