﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities.Commons
{
    public interface IAuditableEntity
    {
        Guid CreateUserID { get; set; }
        Guid? UpdateUserID { get; set; }
        Guid? DeleteUserID { get; set; }
        DateTime CreateTimestamp { get; set; }
        DateTime? UpdateTimestamp { get; set; }
        DateTime? DeleteTimestamp { get; set; }
    }
}
