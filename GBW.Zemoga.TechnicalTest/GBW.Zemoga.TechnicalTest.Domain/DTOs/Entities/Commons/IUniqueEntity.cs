﻿namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities.Commons
{
    public interface IUniqueEntity
    {
        Guid ID { get; set; }
    }
}
