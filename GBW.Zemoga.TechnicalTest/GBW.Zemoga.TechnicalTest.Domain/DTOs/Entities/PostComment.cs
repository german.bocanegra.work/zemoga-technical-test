﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities.Commons;

namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities
{
    public class PostComment : IUniqueEntity, IAuditableEntity, ISortableEntity
    {
        public Guid ID { get; set; }
        public Guid CreateUserID { get; set; }
        public Guid? UpdateUserID { get; set; }
        public Guid? DeleteUserID { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public DateTime? UpdateTimestamp { get; set; }
        public DateTime? DeleteTimestamp { get; set; }
        public int SortOrder { get; set; }

        public Guid PostID { get; set; }
        public Guid CommenterID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public Post? Post { get; set; }
        public User? Commenter { get; set; }

        public PostComment(
            Guid ID, Guid CreateUserID, DateTime CreateTimestamp, int SortOrder, 
            Guid PostID, Guid CommenterID, string Title, string Content)
        {
            this.ID = ID;
            this.CreateUserID = CreateUserID;
            this.CreateTimestamp = CreateTimestamp;
            this.SortOrder = SortOrder;
            this.PostID = PostID;
            this.CommenterID = CommenterID;
            this.Title = Title;
            this.Content = Content;
        }

        public PostComment()
        {
            Title = string.Empty;
            Content = string.Empty;
        }
    }
}
