﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities.Commons;

namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities
{
    public class UserRole : IUniqueEntity, IAuditableEntity
    {
        public Guid ID { get; set; }
        public Guid CreateUserID { get; set; }
        public Guid? UpdateUserID { get; set; }
        public Guid? DeleteUserID { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public DateTime? UpdateTimestamp { get; set; }
        public DateTime? DeleteTimestamp { get; set; }

        public string Name { get; set; }

        public UserRole(Guid ID, string Name)
        {
            this.ID = ID;
            this.Name = Name;
        }

        public UserRole()
        {
            Name = string.Empty;
        }
    }
}
