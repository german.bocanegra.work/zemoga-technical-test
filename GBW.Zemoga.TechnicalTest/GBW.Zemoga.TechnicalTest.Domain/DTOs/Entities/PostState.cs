﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities.Commons;

namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities
{
    public class PostState : IUniqueEntity, IAuditableEntity
    {
        public Guid ID { get; set; }
        public Guid CreateUserID { get; set; }
        public Guid? UpdateUserID { get; set; }
        public Guid? DeleteUserID { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public DateTime? UpdateTimestamp { get; set; }
        public DateTime? DeleteTimestamp { get; set; }

        public string Name { get; set; }

        public PostState(Guid ID, string Name)
        {
            this.ID = ID;
            this.Name = Name;
        }

        public PostState()
        {
            Name = string.Empty;
        }
    }
}
