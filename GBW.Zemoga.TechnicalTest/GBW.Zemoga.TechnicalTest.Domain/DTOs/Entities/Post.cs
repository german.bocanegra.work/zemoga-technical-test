﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities.Commons;

namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities
{
    public class Post : IUniqueEntity, IAuditableEntity, ISortableEntity
    {
        public Guid ID { get; set; }
        public Guid CreateUserID { get; set; }
        public Guid? UpdateUserID { get; set; }
        public Guid? DeleteUserID { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public DateTime? UpdateTimestamp { get; set; }
        public DateTime? DeleteTimestamp { get; set; }
        public int SortOrder { get; set; }

        public Guid OwnerUserID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public Guid StatusID { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? PublishDate { get; set; }

        public User? OwnerUser { get; set; }
        public ICollection<PostReview>? Reviews { get; set; }
        public ICollection<PostComment>? Comments { get; set; }

        public Post(
            Guid ID, Guid CreateUserID, DateTime CreateTimestamp, int SortOrder, 
            Guid OwnerUserID, string Title, string Content, Guid StatusID)
        {
            this.ID = ID;
            this.CreateUserID = CreateUserID;
            this.CreateTimestamp = CreateTimestamp;
            this.SortOrder = SortOrder;
            this.OwnerUserID = OwnerUserID;
            this.Title = Title;
            this.Content = Content;
            this.StatusID = StatusID;
        }

        public Post()
        {
            Title = string.Empty;
            Content = string.Empty;
        }
    }
}
