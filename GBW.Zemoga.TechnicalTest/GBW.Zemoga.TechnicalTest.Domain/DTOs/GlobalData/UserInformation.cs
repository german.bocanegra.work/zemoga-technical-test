﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;

namespace GBW.Zemoga.TechnicalTest.Domain.DTOs.GlobalData
{
    public class UserInformation
    {
        public Guid AccessTokenID { get; set; }
        public User? User { get; set; }

        public UserInformation()
        {
            AccessTokenID = Guid.Empty;
            User = null;
        }
    }
}
