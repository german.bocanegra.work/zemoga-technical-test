﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.Posts;
using GBW.Zemoga.TechnicalTest.Domain.Mappers.PostComments;
using GBW.Zemoga.TechnicalTest.Domain.Mappers.PostReviews;

namespace GBW.Zemoga.TechnicalTest.Domain.Mappers.Posts
{
    public class PostMapper
    {
        public static PostResponse Map(Post post, bool mapReviews = false, bool mapComments = true)
        {
            var postResponse = new PostResponse
            {
                ID = post.ID,
                CreateUserID = post.CreateUserID,
                CreateTimestamp = post.CreateTimestamp,
                DeleteTimestamp = post.DeleteTimestamp,

                OwnerUserID = post.OwnerUserID,
                OwnerUserName = post.OwnerUser != null ? post.OwnerUser.UserName : null,
                Title = post.Title,
                Content = post.Content,
                StatusID = post.StatusID,
                StatusName = PostStateConstants.GetStatusNameByID(post.StatusID),
                SubmitDate = post.SubmitDate,
                PublishDate = post.PublishDate,
                SortOrder = post.SortOrder
            };

            if (mapReviews && post.Reviews != null && post.Reviews.Any())
            {
                foreach (var review in post.Reviews)
                {
                    postResponse.Reviews.Add(PostReviewMapper.Map(review));
                }
            }

            if (mapComments && post.Comments != null && post.Comments.Any())
            {
                foreach (var comment in post.Comments)
                {
                    postResponse.Comments.Add(PostCommentMapper.Map(comment));
                }
            }

            return postResponse;
        }
    }
}
