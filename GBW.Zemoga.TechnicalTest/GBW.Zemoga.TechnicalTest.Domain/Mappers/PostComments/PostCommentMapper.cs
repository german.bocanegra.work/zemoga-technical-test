﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.PostComments;

namespace GBW.Zemoga.TechnicalTest.Domain.Mappers.PostComments
{
    public class PostCommentMapper
    {
        public static PostCommentResponse Map(PostComment postComment)
        {
            var result = new PostCommentResponse
            {
                ID = postComment.ID,
                CreateUserID = postComment.CreateUserID,
                CreateTimestamp = postComment.CreateTimestamp,
                DeleteTimestamp = postComment.DeleteTimestamp,

                CommenterID = postComment.CommenterID,
                CommenterUserName = postComment.Commenter != null ? postComment.Commenter.UserName : null,
                Title = postComment.Title,
                Content = postComment.Content 
            };

            return result;
        }
    }
}
