﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.PostReviews;

namespace GBW.Zemoga.TechnicalTest.Domain.Mappers.PostReviews
{
    public class PostReviewMapper
    {
        public static PostReviewResponse Map(PostReview postReview)
        {
            var result = new PostReviewResponse
            {
                ID = postReview.ID,
                CreateUserID = postReview.CreateUserID,
                CreateTimestamp = postReview.CreateTimestamp,
                DeleteTimestamp = postReview.DeleteTimestamp,

                PostID = postReview.PostID,
                ReviewerID = postReview.ReviewerID,
                ReviewerUserName = postReview.Reviewer != null ? postReview.Reviewer.UserName : null,
                ActionID = postReview.ActionID,
                ActionName = PostReviewActionConstants.GetActionNameByID(postReview.ActionID),
                RejectionComment = postReview.RejectionComment
            };

            return result;
        }
    }
}
