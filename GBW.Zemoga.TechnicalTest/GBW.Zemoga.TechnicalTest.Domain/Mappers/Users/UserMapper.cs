﻿using GBW.Zemoga.TechnicalTest.Domain.Constants;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.Users;

namespace GBW.Zemoga.TechnicalTest.Domain.Mappers.Users
{
    public class UserMapper
    {
        public static UserResponse Map(User user)
        {
            var response = new UserResponse
            {
                ID = user.ID,
                CreateUserID = user.CreateUserID,
                CreateTimestamp = user.CreateTimestamp,
                DeleteTimestamp = user.DeleteTimestamp,
                UserName = user.UserName,
                DisplayName = user.DisplayName,
                RoleID = user.RoleID,
                RoleName = UserRolesConstants.GetRoleNameByID(user.RoleID),
                IsActive = user.IsActive,
                SortOrder = user.SortOrder
            };

            return response;
        }
    }
}
