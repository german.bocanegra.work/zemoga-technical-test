﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.Repositories;

namespace GBW.Zemoga.TechnicalTest.Domain.UnitsOfWork
{
    public interface IUnitOfWork
    {
        public IRepository<Post> Posts { get; }
        public IRepository<PostComment> PostComments { get; }
        public IRepository<PostReview> PostReviews { get; }
        public IRepository<User> Users { get; }

        public IRepository<PostState> PostStates { get; }
        public IRepository<PostReviewAction> PostReviewActions { get; }
        public IRepository<UserRole> UserRoles { get; }
        public IRepository<AccessToken> AccessTokens { get; }

        Task<int> SaveChangesAsync();
    }
}
