﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Users;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.Users;

namespace GBW.Zemoga.TechnicalTest.Domain.Services
{
    public interface IUserService
    {
        Task<UserResponse> GetUserAsync(Guid ID);
        Task<ICollection<UserResponse>> ListUsersAsync(ListUsersRequest request);

        Task<UserResponse> CreateUserAsync(CreateUserRequest request);
        Task<UserResponse> UpdateUserAsync(UpdateUserRequest request);
        Task<UserResponse> DeleteUserAsync(Guid ID);
    }
}
