﻿namespace GBW.Zemoga.TechnicalTest.Domain.Services
{
    public interface IAuthenticationService
    {
        Task<string> CreateAccessToken(string userName, string userPassword);
        Task<string> RefreshAccessToken(string accessToken);
    }
}
