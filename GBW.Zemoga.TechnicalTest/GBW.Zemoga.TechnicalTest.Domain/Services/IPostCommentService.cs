﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostComments;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.PostComments;

namespace GBW.Zemoga.TechnicalTest.Domain.Services
{
    public interface IPostCommentService
    {
        Task<PostCommentResponse> GetPostCommentAsync(Guid ID);
        Task<ICollection<PostCommentResponse>> ListPostCommentsAsync(ListPostCommentRequest request);

        Task<PostCommentResponse> CreatePostCommentAsync(CreatePostCommentRequest request);
        Task<PostCommentResponse> UpdatePostCommentAsync(UpdatePostCommentRequest request);
        Task<PostCommentResponse> DeletePostCommentAsync(Guid ID);
    }
}
