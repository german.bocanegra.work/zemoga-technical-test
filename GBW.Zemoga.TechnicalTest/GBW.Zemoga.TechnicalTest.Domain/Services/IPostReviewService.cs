﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostReviews;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.PostReviews;

namespace GBW.Zemoga.TechnicalTest.Domain.Services
{
    public interface IPostReviewService
    {
        Task<PostReviewResponse> GetPostReviewAsync(Guid ID);
        Task<ICollection<PostReviewResponse>> ListPostReviewsAsync(ListPostReviewsRequest request);
        Task<PostReviewResponse> ReviewPostAsync(ReviewPostRequest request);
    }
}
