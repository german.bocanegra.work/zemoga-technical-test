﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Posts;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Responses.Services.Posts;

namespace GBW.Zemoga.TechnicalTest.Domain.Services
{
    public interface IPostService
    {
        Task<PostResponse> GetPostAsync(Guid ID);
        Task<ICollection<PostResponse>> ListPostsAsync(ListPostsRequest request);

        Task<PostResponse> CreatePostAsync(CreatePostRequest request);
        Task<PostResponse> UpdatePostAsync(UpdatePostRequest request);
        Task<PostResponse> DeletePostAsync(Guid ID);
        Task<PostResponse> SubmitPostAsync(Guid ID);
    }
}
