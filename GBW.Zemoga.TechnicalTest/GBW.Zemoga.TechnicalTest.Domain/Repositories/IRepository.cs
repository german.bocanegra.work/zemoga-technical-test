﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities.Commons;
using System.Linq.Expressions;

namespace GBW.Zemoga.TechnicalTest.Domain.Repositories
{
    public interface IRepository<T> where T : IUniqueEntity, IAuditableEntity
    {
        Task<T?> GetByIDAsync(Guid ID, string propertiesToEagerLoad = "");
        Task<ICollection<T>> GetListAsync(
            Expression<Func<T, bool>>? queryExpression = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderingFunction = null,
            string propertiesToEagerLoad = "",
            bool getDeletedOnly = false,
            int take = 0,
            int skip = 0);

        T Insert(T entity);
        T Update(T entity);
        T Delete(T entity, bool hardDelete = false);
    }
}
