﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Posts;

namespace GBW.Zemoga.TechnicalTest.Domain.Validators
{
    public interface IPostServiceValidator
    {
        void ValidateGetPostRequest(Guid ID);

        Task ValidateListPostRequestAsync(ListPostsRequest request);

        void ValidateCreatePostRequest(CreatePostRequest request);

        void ValidateUpdatePostRequest(UpdatePostRequest request);

        void ValidateDeletePostRequest(Guid ID);
    }
}
