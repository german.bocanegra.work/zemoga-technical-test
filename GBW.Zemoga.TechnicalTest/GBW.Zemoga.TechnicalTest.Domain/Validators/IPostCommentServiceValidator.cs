﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostComments;

namespace GBW.Zemoga.TechnicalTest.Domain.Validators
{
    public interface IPostCommentServiceValidator
    {
        void ValidateGetPostCommentRequest(Guid ID);

        Task ValidateListPostCommentRequestAsync(ListPostCommentRequest request);

        Task ValidateCreatePostCommentRequestAsync(CreatePostCommentRequest request);

        void ValidateUpdatePostCommentRequest(UpdatePostCommentRequest request);

        void ValidateDeletePostCommentRequest(Guid ID);
    }
}
