﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Entities;
using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.PostReviews;

namespace GBW.Zemoga.TechnicalTest.Domain.Validators
{
    public interface IPostReviewServiceValidator
    {
        void ValidateGetPostReviewRequest(Guid ID);

        Task ValidateListPostReviewRequestAsync(ListPostReviewsRequest request);

        void ValidateReviewPostRequest(ReviewPostRequest request, Post post);
    }
}
