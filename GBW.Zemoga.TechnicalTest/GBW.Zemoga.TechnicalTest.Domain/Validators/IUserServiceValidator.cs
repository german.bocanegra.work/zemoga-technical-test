﻿using GBW.Zemoga.TechnicalTest.Domain.DTOs.Requests.Users;

namespace GBW.Zemoga.TechnicalTest.Domain.Validators
{
    public interface IUserServiceValidator
    {
        void ValidateGetUserRequest(Guid ID);

        void ValidateListUserRequest(ListUsersRequest request);

        void ValidateCreateUserRequest(CreateUserRequest request);

        void ValidateUpdateUserRequest(UpdateUserRequest request);

        void ValidateDeleteUserRequest(Guid ID);
    }
}
