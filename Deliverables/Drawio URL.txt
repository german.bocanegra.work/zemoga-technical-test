Diagrams can be viewed on the free web app or the desktop app:

	This URL:
		https://www.draw.io
		
	Will rediredirect you to:
		https://app.diagrams.net (they seem to be rebranding)
	
	To use the old URL go to the site using this:
		https://www.draw.io/index.html
		
Note: I like this tool because of it's simplicity.