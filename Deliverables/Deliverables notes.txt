Deliverables:

	Development time:
		- About 2 hours of planning
		- Around 16 hours implementing the entire solution
		- About 2 hours configuring the test classes and writing the sample Unit test
		- Around 3 hours creating all the AWS resources and deploying the application
		
		Total: 23 hours / close to 3 full work days
		Note: I put a lot of effort into this test, I hope it shows :)

	Repository URL: https://gitlab.com/german.bocanegra.work/zemoga-technical-test
	Repository clone URL: https://gitlab.com/german.bocanegra.work/zemoga-technical-test.git
		Note: the repository is public.
		
	Basic instructions for API usage:
		1. Get an access token by calling the Auth/CreateAccessToken endpoint (requires a valid username and password).
		2. Store the AccessToken you just generated in the {{AccessToken}} postman variable (authorization is already configured with bearer).
		3. Use the other endpoints as pleased (role based validations are in place, most endpoints are only available for the administrator.
		Note: The main endpoints that would've been used in the frontend application are:
			Auth / generate access token
			Auth / refresh access token (creates a new access token using the currently authorized user without providing credentials again)
			
			Posts / List posts published - For any user
			Posts / List posts owner - For writer, returns only their own posts
			Posts / List posts pending - For editor, returns only submitted (pending) posts
			
			Post / Create post - Writer only
			Post / Update post - Writer only
			
			Post / Review post - Editor only
			
			PostComment / Create comment - For any user, only on published (approved) posts
			
		All other endpoints are only available for the administrator, mostly CRUD operations that don't directly belong to any role.
		
	Postman collection: Included as JSON
		Note: remember to update the variable with the host URL (if pointing to AWS or local) and the bearer auth token.
		Note: the collection sent is already pointing to the AWS deployed containers.
		Note: Variables and authorization are configured on the Collection, not the folders or requests.
		
	Public AWS API Host: http://laboratory-1734031649.us-east-1.elb.amazonaws.com
		Note: does not have https configured, here I would've done it using AWS application load balancer.
		Note: The API was deployed using AWS ECS with 2 containers and an application load balancer in front of it, 
			the database was created in RDS using EF migrations.
		
	Health check endpoint: http://laboratory-1734031649.us-east-1.elb.amazonaws.com/api/auth/healthcheck
	
	Swagger URL: https://localhost/swagger/index.html
		Note: Swagger is left blocked in the public URI by design, it is only available for local environments.
	
	Note: I skipped writing comments for most code by choice, I prefer to write readable code that documents itself, I hope I achieved my goal in this test.
	
	Note: I skipped writing most unit tests because of time constraints, I added a single test class to demonstrate the way i write them.
	Note: The test I wrote isn't particularly effective, it's only to show my understanding of mocks and other testing tools, 
		I usually need to put some thought and time into designing (and then writing) good unit tests.
	
	Users: Default admin:
		User: dev_admin
		Password: 56f862b6-133d-4a21-a3e0-41206ba325ea
		
		Other users:
			User: writer_1
			Password: 1
			
			User: writer_2
			Password: 2
			
			
			User: editor_1
			Password: 3
			
			User: editor_2
			Password: 4
			
			
			User: public_1
			Password: 5
			
			User: public_2
			Password: 6