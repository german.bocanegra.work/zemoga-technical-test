# zemoga-technical-test
	Technical test for Zemoga Application - Blog engine API

## Instructions

	Create database server - Do according to your platform selection, install MySQL 8.0.32 (community edition is fine)
	Configure connectivity - Do according to your platform selection, unnecessary in most local environments
	Create database
		DROP DATABASE IF EXISTS zemoga_blog_engine; - If you want a fresh install after migrations fail
		CREATE DATABASE IF NOT EXISTS zemoga_blog_engine;
	Create application User
		USE zemoga_blog_engine;
		DROP USER IF EXISTS 'zemoga_api'@'%';
		CREATE USER 'zemoga_api'@'%' IDENTIFIED BY 'as123lkd57j3kafn6aojgasldkjui';
		
		Note: this is a master user only meant for development, these permissions are too broad for a production environment
		GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, RELOAD, PROCESS, REFERENCES, INDEX, ALTER, SHOW DATABASES, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, REPLICATION SLAVE, REPLICATION CLIENT, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, CREATE USER, EVENT, TRIGGER ON 'zemoga_blog_engine'.* TO 'zemoga_api'@'%' WITH GRANT OPTION;
		
		FLUSH PRIVILEGES;

	Go to your working folder
	Clone source code = git clone 
	Fetch remotes = git fetch
	Switch to desired branch (develop) - git switch develop
	Configure environment variables - Do according to your OS
		DB connection string = DB_CONNECTION_STRING='Server=localhost;User ID=zemoga_api;Password=;Database=zemoga_blog_engine'
			Note: replace 'localhost' with your database host name
		DB password = DB_PASSCODE='as123lkd57j3kafn6aojgasldkjui'
			Note: replace this value with the password you assigned to your user
			
	Go to the WebApi project folder where the .csproj file is located
	Install .Net SDK 7 if not installed = sudo yum install dotnet-sdk-7.0
		Note: if on AMZ linux2, run this command to be able to find the package = sudo rpm -Uvh https://packages.microsoft.com/config/centos/7/packages-microsoft-prod.rpm
	Install ef tools if necessary (on AMZ linux2 for example) = dotnet tool install --global dotnet-ef
	Run migrations = dotnet ef database update

	Clean code = dotnet clean
		Note: this is done to keep local binaries minimized, mostly a good practice
	Build code = dotnet build
		Note: this id done to build the code before running, it helps identify compilation errors or warnings

	Run from source code:
		Run code = dotnet run

	Run from published binaries:
		Publish code = dotnet publish -c Release
		Go to publish folder = GBW.Zemoga.TechnicalTest.WebApi/bin/Release/net7.0/publish
		Run code = dotnet GBW.Zemoga.TechnicalTest.WebApi.dll
		
	To build a Docker Image:
		Go to the folder with the SLN file
		Run build command = docker build -t zemoga_test/api -f Dockerfile .
		Note: runs on port 80

## Deliverables

	Development time:
		- About 2 hours of planning
		- Around 16 hours implementing the entire solution
		- About 2 hours configuring the test classes and writing the sample Unit test
		- Around 3 hours creating all the AWS resources and deploying the application
		
		Total: 23 hours / close to 3 full work days
		Note: I put a lot of effort into this test, I hope it shows :)

	Repository URL: https://gitlab.com/german.bocanegra.work/zemoga-technical-test
	Repository clone URL: https://gitlab.com/german.bocanegra.work/zemoga-technical-test.git
		Note: the repository is public.
		
	Basic instructions for API usage:
		1. Get an access token by calling the Auth/CreateAccessToken endpoint (requires a valid username and password).
		2. Store the AccessToken you just generated in the {{AccessToken}} postman variable (authorization is already configured with bearer).
		3. Use the other endpoints as pleased (role based validations are in place, most endpoints are only available for the administrator.
		Note: The main endpoints that would've been used in the frontend application are:
			Auth / generate access token
			Auth / refresh access token (creates a new access token using the currently authorized user without providing credentials again)
			
			Posts / List posts published - For any user
			Posts / List posts owner - For writer, returns only their own posts
			Posts / List posts pending - For editor, returns only submitted (pending) posts
			
			Post / Create post - Writer only
			Post / Update post - Writer only
			
			Post / Review post - Editor only
			
			PostComment / Create comment - For any user, only on published (approved) posts
			
		All other endpoints are only available for the administrator, mostly CRUD operations that don't directly belong to any role.
		
	Postman collection: Included as JSON
		Note: remember to update the variable with the host URL (if pointing to AWS or local) and the bearer auth token.
		Note: the collection sent is already pointing to the AWS deployed containers.
		Note: Variables and authorization are configured on the Collection, not the folders or requests.
		
	Public AWS API Host: http://laboratory-1734031649.us-east-1.elb.amazonaws.com
		Note: does not have https configured, here I would've done it using AWS application load balancer.
		Note: The API was deployed using AWS ECS with 2 containers and an application load balancer in front of it, 
			the database was created in RDS using EF migrations.
		
	Health check endpoint: http://laboratory-1734031649.us-east-1.elb.amazonaws.com/api/auth/healthcheck
	
	Swagger URL: https://localhost/swagger/index.html
		Note: Swagger is left blocked in the public URI by design, it is only available for local environments.
	
	Note: I skipped writing comments for most code by choice, I prefer to write readable code that documents itself, I hope I achieved my goal in this test.
	
	Note: I skipped writing most unit tests because of time constraints, I added a single test class to demonstrate the way i write them.
	Note: The test I wrote isn't particularly effective, it's only to show my understanding of mocks and other testing tools, 
		I usually need to put some thought and time into designing (and then writing) good unit tests.
	
	Users: Default admin:
		User: dev_admin
		Password: 56f862b6-133d-4a21-a3e0-41206ba325ea
		
		Other users:
			User: writer_1
			Password: 1
			
			User: writer_2
			Password: 2
			
			
			User: editor_1
			Password: 3
			
			User: editor_2
			Password: 4
			
			
			User: public_1
			Password: 5
			
			User: public_2
			Password: 6